# LEKKERLEEGLOPEN.nl

# Development

```bash
yarn install
yarn watch
```

The application supports Redux dev tools. [http://extension.remotedev.io/](http://extension.remotedev.io/) 

# Instructions 

There 2 different ways to decorate a message. 

- The complete message.
- Part of the message, like a block of text or a single word.

For both ways you need to tag the message or text on the server side. On the client side you can find 
the assigned tag and decorate accordingly.

## Text

### Server side
 
1. Add tags to the text with your own implementation of a 'TextTagService'. See ```src/server/Tagging/Text``` for examples.
2. Add you service or service instance to the DI container in ```src/server/Dependency/taggedServicesDependencies.ts```.
3. Test if it works with Redux dev tools.    
   ![redux](docs/redux.png "redux") 
4. Add unit test <^-^> in ```src/server/Tagging/Text/__test__```.

### Client side

1. Create the text decorator (TextDecoratorChain). See examples in  ```src/client/Component/Decorater/Text```.
2. Add decorator to the decorator chain in ```src/client/Component/Decorater/TextDecorator.tsx```.
3. Test if it works! :)
4. Add unit test :D in ```src/client/Component/Decorater/Text/__test__```

## Message

### Server side
 
1. Add tags to the message with your own implementation of a 'MessageTagService'. See ```src/server/Tagging/Message``` for examples.
2. Add you service or service instance to the DI container in ```src/server/Dependency/taggedServicesDependencies.ts```.
3. Test if it works with Redux dev tools.
4. Add unit test O_0 in ```src/server/Tagging/Message/__test__```

### Client side

1. Create the message decorator (MessageDecoratorChain). See examples in  ```src/client/Component/Decorater/Message```.
2. Add decorator to the decorator chain in ```src/client/Component/Decorater/MessageDecorator.tsx```.
3. Test if it works! :)
4. Add unit test :D in ```src/client/Component/Decorater/Message/__test__```

## Metadata

When you tag a message or text you can specify metadata, like img urls etc. See ```src/server/Tagging/Text/TextToImage.ts``` for an example.

```typescript
export const ToImageDecorator: TextDecoratorChain = () => (next) => (text) => {
  if (text.tags.contains(TEXT_TAG_TO_IMAGE) && text.metadata.has('img')) {
    return <img className="tag--text-to-img" srcSet={text.metadata.get('img')} alt={text.toString()} />;
  }
  return next(text);
};
```

## Randomness 

Text(TaggedText.ts) and messages (Message.ts) value objects have it own way of creating random number on the client-side. The numbers are random based on a given seed by
the server. Every client then have the same number returned and should have the same visual behavior for that particular part on each client. See ProfanityTextDecorator 
or ClippyDecorator for 2 examples.

```typescript
  <Clippy
    x={text.randomBetween('x', window.innerWidth - 200)}
    y={text.randomBetween('y', window.innerHeight - 200)}
    agent={text.metadata.get('agent')}
    lifetimeSeconds={message.countDownMSPast(60 * 1000)}
  >
    {next(text)}
  </Clippy>);
```

## Timing

If you want to do something based on a time the message is shown, like hiding or disabling stuff. Use the 'timeLeftOfTimer' function for this.

```typescript
<DrawBugs lifetimeLeft={message.timeLeftOfTimer(60 * 1000)}>{next(text)}</DrawBugs>;
```
