/*
 * jQuery Raptorize Plugin 1.0
 * www.ZURB.com/playground
 * Copyright 2010, ZURB
 * Free to use under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 *
 * THIS File is altered for lekkerleeglopen
 */
(function ($) {

    $.fn.raptorize = function () {
        //Extend those options
        var audioSupported = true;

        //Raptor Vars
        var raptorImageMarkup = '<img id="elRaptor" style="display: none" src="/public/raptorize/raptor.png" />'
        var raptorAudioMarkup = '<audio id="elRaptorShriek" preload="auto"><source src="/public/raptorize/raptor-sound.mp3" /><source src="/public/raptorize/raptor-sound.ogg" /></audio>';
        var locked = false;

        //Append Raptor and Style
        $('body').append(raptorImageMarkup);
        if (audioSupported) {
            $('body').append(raptorAudioMarkup);
        }
        var raptor = $('#elRaptor').css({
            "position": "fixed",
            "bottom": "-700px",
            "right": "0",
            "display": "block"
        });

        // Animating Code
        locked = true;

        //Sound Hilarity
        if (audioSupported) {
            function playSound () {
                document.getElementById('elRaptorShriek').play();
            }

            playSound();
        }

        // Movement Hilarity
        raptor.animate({
            "bottom": "0"
        }, function () {
            $(this).animate({
                "bottom": "-130px"
            }, 100, function () {
                var offset = (($(this).position().left) + 400);
                $(this).delay(300).animate({
                    "right": offset
                }, 2200, function () {
                    raptor = $('#elRaptor').css({
                        "bottom": "-700px",
                        "right": "0"
                    })
                    locked = false;
                })
            });
        });

    }//orbit plugin call
})(jQuery);

