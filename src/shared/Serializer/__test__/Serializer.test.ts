import 'jest';
import { Serializer } from '../Serializer';
import { Record } from 'immutable';

describe('Serializer', () => {

  it('Can serialize normal javascript objects', () => {
    const serializer = new Serializer([]);
    const date = new Date();
    date.setTime(1518770045540);
    const target = {
      date,
      arrays: [1, 2, 3],
      objects: { test: 2 },
    };
    const serialized = serializer.serialize(target);
    expect(serialized).toMatchSnapshot();
    const deSerialized = serializer.deserialize(serialized);
    expect(deSerialized).toEqual(target);
  });

  it('Can serialize records', () => {
    const testRecord = Record({ name: 'foo' }, 'test');
    const test = new testRecord({ name: 'bar' });
    const serializer = new Serializer([testRecord]);
    const serialized = serializer.serialize(test);
    expect(serialized).toMatchSnapshot();
    const deSerialized = serializer.deserialize(serialized);
    expect(deSerialized).toEqual(test);
  });

});
