
export interface SerializerInterface {

  serialize(data: any): string;

  deserialize(json: string): any;
}
