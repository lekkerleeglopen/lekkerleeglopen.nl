import * as transit from 'transit-immutable-js';
import { Record } from 'immutable';
import { SerializerInterface } from './SerializerInterface';

interface RecordConstructor {
  new (...args: any[]): Record<any>;
}

export class Serializer implements SerializerInterface {

  private recordTransit;

  constructor(records: RecordConstructor[]) {
    /**
     * Simple map to verify you cannot give a record with the same name.
     */
    const recordsTypes = {};
    records.forEach((record) => {
      const descriptiveName = Record.getDescriptiveName(new record());
      if (descriptiveName === '' || descriptiveName.toLowerCase() === 'record') {
        throw new Error('wrong descriptiveName record name ' + descriptiveName);
      }
      if (typeof recordsTypes[descriptiveName] !== 'undefined') {
        throw new Error('Records with this name already given ' + descriptiveName);
      }
      recordsTypes[descriptiveName] = record;
    });
    this.recordTransit = transit.withRecords(records);
  }

  public serialize(data: any): string {
    return this.recordTransit.toJSON(data);
  }

  public deserialize(json: string): any {
    return this.recordTransit.fromJSON(json);
  }

}
