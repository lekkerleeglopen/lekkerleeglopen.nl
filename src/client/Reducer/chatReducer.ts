import { AnyAction } from 'redux';
import {
  CHAT_RECEIVED_INITIAL_STATE_FROM_SERVER,
  CHAT_RECEIVED_MESSAGE_FROM_SERVER,
  CHAT_REMOVE_OLD_MESSAGES,
} from '../Action/chat';
import { ChatState } from '../State/ChatState';

export function chatReducer(state: ChatState = new ChatState(), action: AnyAction): ChatState {

  switch (action.type) {
    case CHAT_RECEIVED_INITIAL_STATE_FROM_SERVER:
      return action.state;
    case CHAT_RECEIVED_MESSAGE_FROM_SERVER:
      return state.addMessage(action.message);
    case CHAT_REMOVE_OLD_MESSAGES:
      return state.removeOldMessages(action.date);
  }

  return state;
}
