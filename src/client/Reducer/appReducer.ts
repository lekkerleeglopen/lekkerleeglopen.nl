import { AnyAction } from 'redux';
import { AppState } from '../State/AppState';
import { APP_SOCKET_CONNECTED, APP_SOCKET_DISCONNECTED, APP_USER_UPDATES_PENDING_MESSAGE } from '../Action/app';
import { CHAT_SEND_MESSAGE_TO_SERVER } from '../Action/chat';

export function appReducer(state: AppState = new AppState(), action: AnyAction): AppState {

  switch (action.type) {
    case APP_SOCKET_CONNECTED:
      return state.set('socketConnected', true);
    case APP_SOCKET_DISCONNECTED:
      return state.set('socketConnected', false);
    case APP_USER_UPDATES_PENDING_MESSAGE:
      return state.set('message', action.message);
    case CHAT_SEND_MESSAGE_TO_SERVER:
      return state.set('message', '');
  }

  return state;
}
