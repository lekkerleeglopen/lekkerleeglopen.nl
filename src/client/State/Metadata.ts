import { Map } from 'immutable';

export type Metadata = Map<string, any>;
