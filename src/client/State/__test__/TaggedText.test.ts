import 'jest';
import { List, Set, Map } from 'immutable';
import { TaggedText } from '../TaggedText';

describe('TaggedText', () => {

  it('Can add tags', () => {
    const text = new TaggedText({ parts: List(['Some text']) });
    expect(text.addTags(Set(['test'])).tags.toJS()).toEqual(['test']);
  });

  it('Add tag to the complete text', () => {
    const text = 'Some text';
    const taggedText = new TaggedText({
      parts: List([text]),
    });
    const result = taggedText.tagSlice(0, text.length, Set(['test']), Map({}));
    expect(result.tags).toEqual(Set(['test']));

  });

});
