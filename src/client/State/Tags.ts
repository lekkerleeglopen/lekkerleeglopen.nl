import { Set } from 'immutable';

export type Tags = Set<string>;
