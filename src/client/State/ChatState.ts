import { List, Record } from 'immutable';
import { Message } from './Message';

interface ChatStateInterface {
  messages: List<Message>;
}

const defaultChatState: ChatStateInterface = {
  messages: List<Message>(),
};

/**
 * This state will be managed server side and client side.
 */
export class ChatState extends Record(defaultChatState, 'ChatState') {
  public readonly messages: List<Message>;

  public addMessage(message: any): this {
    return this.set('messages', this.messages.unshift(message).slice(0, 50));
  }

  public removeOldMessages(date: Date): this {
    const time = date.getTime();

    return this.set('messages', this.messages.filter((message) => {
      const diffMs = time - message.postedOn.getTime();
      const diffMinutes = (diffMs / 1000) / 60;
      return diffMinutes < 20;
    }));
  }
}
