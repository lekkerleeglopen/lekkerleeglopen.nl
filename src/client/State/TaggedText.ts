import { List, Record, Set, Map } from 'immutable';
import { Metadata } from './Metadata';
import { Tags } from './Tags';
import * as seedrandom from 'seedrandom';

interface TaggedTextInterface {
  parts?: List<TaggedText | string>;
  tags?: Tags;
  metadata?: Metadata;
  random?: number;
}

const defaultTaggedText: TaggedTextInterface = {
  parts: List(),
  tags: Set(),
  metadata: Map<string, any>(),
  random: null,
};

/**
 * The idea is that the TaggedText is a classification tree.
 *
 * 'Javascript is stupid'
 *
 * Can become:
 *
 * TaggedText {
 *    tags: ['javascript']
 *    parts: [
 *      'Javascript is ',
 *      TaggedText {
 *          parts: ['stupid'],
 *          tags: ['swear word', 'lame'],
 *      }
 *    ]
 * }
 */
export class TaggedText extends Record(defaultTaggedText, 'TaggedText') {

  public readonly parts: List<TaggedText | string>;
  public readonly tags: Tags;
  public readonly metadata: Metadata;

  constructor(values?: TaggedTextInterface) {
    if (values && typeof values.random === 'undefined') {
      values.random = Math.random();
    }
    super(values)
  }

  public toString() {
    return this.parts.join('');
  }

  /**
   * This random value generator is always the same for all clients.
   */
  public randomBetween(seed: string, max: number, min = 0) {
    const arng = seedrandom.alea(seed + this.random);
    return Math.floor(arng() * max) + min;
  }

  public addTags(tags: Tags): this {
    return this.set('tags', this.tags.merge(tags));
  }

  public mergeMetadata(metadata: Metadata): this {
    return this.set('metadata', this.metadata.merge(metadata));
  }

  /**
   * Returns all tags including nested parts.
   */
  public getAllTags(): Tags {
    let tags = this.tags;
    this.parts.forEach((part) => {
      if (part instanceof TaggedText) {
        tags = tags.concat(part.getAllTags())
      }
    });
    return tags;
  }

  /**
   * Flatten this tagged text.
   *
   * If this tagged text, contains one tagged text part, return that part and combine
   * tags and metadata.
   */
  public flatten(): TaggedText {
    const first = this.parts.first();
    if (this.parts.count() === 1 && first instanceof TaggedText) {
      return first.addTags(this.tags).mergeMetadata(this.metadata);
    }
    return this;
  }

  /**
   * Remove empty text parts
   */
  public withoutEmptyTextParts(): TaggedText {
    return this.set('parts', this.parts.filter((part) => {
      return part.toString() !== '';
    }));
  }

  /**
   * Remove random value, for testing purposes.
   */
  public withoutRandom(): TaggedText {
    return this
      .set('random', 0)
      .set('parts', this.parts.map((part) => {
        if (typeof part === 'string') {
          return part;
        }
        return part.withoutRandom();
      }));
  }

  /* tslint:disable:no-parameter-reassignment */
  /**
   * Recursive function to go down the tree, to tag a slice of text.
   *
   */
  public tagSlice(
    start: number,
    end: number,
    tags: Tags,
    metadata: Metadata): TaggedText {
    let partBegin = 0;
    let partEnd = 0;
    const parts = this.parts.map((part) => {
      partEnd += part.toString().length;
      // Check if this part matches the slice.
      if (partBegin === start && partEnd === end) {
        part = this.convertToTaggedText(part)
          .addTags(tags)
          .mergeMetadata(metadata);

        // It in a child.
      } else if (partBegin <= start && partEnd >= end) {
        if (typeof part === 'string') {
          const startRelativeToPart: number = start - partBegin;
          const endRelativeToPart: number = end - partBegin;
          const textFirstPart = part.slice(0, startRelativeToPart);
          const needle = part.slice(startRelativeToPart, endRelativeToPart);
          const textEndText = part.slice(endRelativeToPart);
          const tagged = new TaggedText({
            parts: List([needle]),
            tags,
            metadata,
          });
          part = new TaggedText({
            parts: List([
              textFirstPart,
              tagged,
              textEndText,
            ]),
          });
          // Parts can be empty, remove them.
          part = part.withoutEmptyTextParts().flatten();
        } else {
          part = part.tagSlice(start - partBegin, end - partBegin, tags, metadata);
        }
      }
      partBegin = partEnd;
      return part;
    });
    return this.set('parts', parts).withoutEmptyTextParts().flatten();
  }

  private convertToTaggedText(text: TaggedText | string): TaggedText {
    if (!(text instanceof TaggedText)) {
      return new TaggedText({ parts: List([text]) })
    }
    return text;
  }

}
