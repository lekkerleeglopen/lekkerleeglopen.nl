
import { ChatState } from './ChatState';
import { AppState } from './AppState';

export interface ClientState {
  chat: ChatState;
  app: AppState;
}
