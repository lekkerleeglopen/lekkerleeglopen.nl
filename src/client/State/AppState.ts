import { Record } from 'immutable';

interface AppStateInterface {
  socketConnected: boolean;
  message: string;
}

const defaultAppState: AppStateInterface = {
  socketConnected: false,
  message: '',
};

/**
 * This state will be only be available on client side
 */
export class AppState extends Record(defaultAppState, 'AppState') {
  public readonly socketConnected: boolean;
  public readonly message: string;
}
