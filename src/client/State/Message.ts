import { Set, Record, Map } from 'immutable';
import { TaggedText } from './TaggedText';
import * as seedrandom from 'seedrandom';

interface MessageInterface {
  uuid: string;
  random?: number;
  postedOn: Date;
  text: TaggedText;
  tags: Set<string>,
  metadata: Map<string, any>,
}

const defaultMessage: MessageInterface = {
  uuid: null,
  postedOn: null,
  random: null,
  text: null,
  tags: Set([]),
  metadata: Map<string, any>(),
};

export class Message extends Record(defaultMessage, 'Message') {
  public readonly uuid: string;
  public readonly postedOn: Date;
  public readonly text: TaggedText;
  public readonly tags: Set<string>;
  public readonly metadata: Map<string, any>;

  constructor(values?: MessageInterface) {
    if (values && !values.random) {
      values.random = Math.random();
    }
    super(values)
  }

  public msPast(date = new Date()) {
    return (date.getTime() - this.postedOn.getTime());
  }

  /**
   * How many ms are left when countdown has past.
   */
  public timeLeftOfTimer(duration: number, date = new Date()) {
    const seconds = this.msPast(date);
    const left = duration - seconds;
    return left < 0 ? 0 : left;
  }

  /**
   * This random value generator is always the same for all clients.
   */
  public randomBetween(seed: string, max: number, min = 0) {
    const arng = seedrandom.alea(seed + this.random);

    return Math.floor(arng() * max) + min;
  }

}
