import 'rxjs/add/operator/ignoreElements';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import { CHAT_RECEIVED_MESSAGE_FROM_SERVER } from '../Action/chat';
import { AnyAction } from 'redux';
import { ActionsObservable } from 'redux-observable';
import { Observable } from 'rxjs/Observable';
import { Message } from '../State/Message';
import { MESSAGE_TAG_WOW } from '../../server/Dependency/Tags';

export const wowEpic = (action$: ActionsObservable<AnyAction>) => {
  const messageFromServer$: Observable<Message> = action$
    .ofType(CHAT_RECEIVED_MESSAGE_FROM_SERVER)
    .map(({ message }) => message);

  return messageFromServer$
    .filter((message: Message) => message.tags.contains(MESSAGE_TAG_WOW))
    .do(() => {
      try {
        const nr = Math.floor(Math.random() * 4) + 1;
        const date = new Date();
        const time = date.getTime();
        const wowAudioMarkup = '<audio id="wowElement' + time + '" preload="auto"><source src="/public/sound/wow' + nr + '.mp3" /></audio>';
        (jQuery('body') as any).append(wowAudioMarkup);
        (document.getElementById('wowElement' + time) as any).play();
      } catch (e) {
        // Do nothing.
      }
    })
    // Share a single stream over multiple subscribers.
    .ignoreElements() as any;
};
