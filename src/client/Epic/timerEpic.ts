import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/map';
import { chatRemoveOldMessages } from '../Action/chat';

export const timerEpic = () => {
  return Observable
    // Send a action to remove messages every 30 seconds.
    .interval(30 * 1000)
    // Share a single stream over multiple subscribers.
    .map(() => chatRemoveOldMessages());
};
