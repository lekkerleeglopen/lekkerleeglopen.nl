import 'rxjs/add/operator/ignoreElements';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/distinct';
import 'rxjs/add/operator/filter';
import { CHAT_RECEIVED_MESSAGE_FROM_SERVER } from '../Action/chat';
import { AnyAction } from 'redux';
import { ActionsObservable } from 'redux-observable';
import { Observable } from 'rxjs/Observable';
import { Message } from '../State/Message';
import { TEXT_TAG_CLIPPY, MESSAGE_TAG_CLIPPY_ANIMATE } from '../../server/Dependency/Tags';
import { Clippy } from '../Component/Decorater/Text/ClippyDecorator';

declare var clippies: Clippy[];

export const clippyEpic = (action$: ActionsObservable<AnyAction>) => {
  (window as any).clippies = [];
  const messageFromServer$: Observable<Message> = action$
    .ofType(CHAT_RECEIVED_MESSAGE_FROM_SERVER)
    .map(({ message }) => message);

  return messageFromServer$
    .filter((message: Message) => message.tags.contains(MESSAGE_TAG_CLIPPY_ANIMATE))
    .do((message) => {
      clippies.forEach((clippy) => {
        clippy.animate(message.metadata.get('animation'));
      });
    })
    .merge(
      messageFromServer$
        .filter((message: Message) => !message.text.getAllTags().contains(TEXT_TAG_CLIPPY) && !message.tags.contains(MESSAGE_TAG_CLIPPY_ANIMATE))
        .do((message) => {
          clippies.forEach((clippy) => {
            clippy.speak(message.text.toString());
          });
        }),
    )
    // Share a single stream over multiple subscribers.
    .ignoreElements() as any;
};
