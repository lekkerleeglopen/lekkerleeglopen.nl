import 'rxjs/add/operator/ignoreElements';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import { CHAT_RECEIVED_MESSAGE_FROM_SERVER } from '../Action/chat';
import { AnyAction } from 'redux';
import { ActionsObservable } from 'redux-observable';
import { Observable } from 'rxjs/Observable';
import { Message } from '../State/Message';
import { MESSAGE_TAG_RAPTOR } from '../../server/Dependency/Tags';

export const raptorEpic = (action$: ActionsObservable<AnyAction>) => {
  const messageFromServer$: Observable<Message> = action$
    .ofType(CHAT_RECEIVED_MESSAGE_FROM_SERVER)
    .map(({ message }) => message);

  return messageFromServer$
    .filter((message: Message) => message.tags.contains(MESSAGE_TAG_RAPTOR))
    .do(() => {
      try {
        // Make sure the application does not break;
        (jQuery.fn as any).raptorize();
      } catch (e) {
        // Do nothing.
      }
    })
    // Share a single stream over multiple subscribers.
    .ignoreElements() as any;
};
