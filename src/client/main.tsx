/**
 * Client bootstrap file.
 */

import 'babel-polyfill';
import * as jQuery from 'jquery';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ClientDependencyContainer } from './ClientDependencyContainer';
import { IndexViewConnected } from './View/IndexView';

const container = new ClientDependencyContainer();

// Add jquery as global, for old jquery libraries.
(window as any).jQuery = jQuery;
(window as any).$ = jQuery;

container.socketMessageService().connect();

const elementById = document.getElementById('app');
ReactDOM.render(
  (
    <Provider store={container.store()}>
      <IndexViewConnected/>
    </Provider>
  ),
  elementById,
);
