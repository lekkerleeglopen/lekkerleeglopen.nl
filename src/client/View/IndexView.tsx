/**
 * Component.
 */

import * as React from 'react';
import { connect } from 'react-redux';
import { ChatMessageInputConnected } from '../Component/Input/ChatMessageInputConnected';
import { ChatFeedConnected } from '../Component/Feed/ChatFeedConnected';

export class IndexView extends React.PureComponent {

  public render(): React.ReactNode {
    return (
      <div>
        <ChatMessageInputConnected/>
        <ChatFeedConnected/>
      </div>
    );
  }
}

export const IndexViewConnected = connect()(IndexView);
