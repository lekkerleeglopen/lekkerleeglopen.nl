/**
 * Simple implementation of di container.
 */
export class DependencyContainer {

  private compiled: boolean = false;
  private cache: { [key: string]: any } = {};

  constructor() {
    this.compile();
  }

  protected compile() {
    if (this.compiled) {
      throw new Error('Container already compiled');
    }
    this.compiled = true;
    const ownPropertyNames = Object.getOwnPropertyNames(Object.getPrototypeOf(this));
    const ownFunctions = ownPropertyNames.filter((property) => typeof this[property] === 'function' && property !== 'constructor');
    ownFunctions.forEach((functionName) => {
      const factoryFunction = this[functionName];
      this[functionName] = (...args: any[]) => {
        const cacheKey = args.concat([functionName]).join('|');
        if (!this.cache[cacheKey]) {
          this.cache[cacheKey] = factoryFunction.apply(this, args);
        }
        return this.cache[cacheKey];
      };
    });
  }

}
