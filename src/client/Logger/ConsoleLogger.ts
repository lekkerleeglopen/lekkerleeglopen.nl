import { LoggerInterface } from './LoggerInterface';

export class ConsoleLogger implements LoggerInterface {

  public debug(...message: any[]) {
    this.call('info', arguments);
  }

  public info(...message: any[]) {
    this.call('info', arguments);
  }

  public warning(...message: any[]) {
    this.call('warning', arguments);
  }

  public error(...message: any[]) {
    this.call('error', arguments);
  }

  private call(type, args) {
    /* tslint-disable no-console */
    if (!console || !console[type]) {
      return;
    }
    try {
      console[type].apply(console, args);
    } catch (e) {
      // No clue.
    }
    /* tslint-enable no-console */
  }

}
