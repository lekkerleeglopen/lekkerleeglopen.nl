export interface LoggerInterface {
  debug(...message: any[]);

  info(...message: any[]);

  warning(...message: any[]);

  error(...message: any[]);
}
