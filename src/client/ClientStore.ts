
import { Store } from 'redux';
import { ClientState } from './State/ClientState';

export interface ClientStore extends Store<ClientState> {}
