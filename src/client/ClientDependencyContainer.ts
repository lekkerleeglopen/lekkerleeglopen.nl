import { DependencyContainer } from './DependencyContainer';
import { StoreFactory } from './StoreFactory';
import { ChatSocketService } from './Socket/ChatSocketService';
import { SerializerFactory } from './SerializerFactory';
import { LoggerInterface } from './Logger/LoggerInterface';
import { ConsoleLogger } from './Logger/ConsoleLogger';
import { chatSocketMiddleware } from './Middleware/chatSocketMiddleware';
import { multipleMiddleware } from './Middleware/multipleMiddleware';
import { promiseMiddleware } from './Middleware/promiseMiddleware';
import { storeMiddleware } from './Middleware/storeMiddleware';
import { ignoreMiddleware } from './Middleware/ignoreMiddleware';
import { combineEpics, createEpicMiddleware } from 'redux-observable';
import { timerEpic } from './Epic/timerEpic';
import { Middleware } from 'redux';
import { raptorEpic } from './Epic/raptorEpic';
import { wowEpic } from './Epic/wowEpic';
import { clippyEpic } from './Epic/clippyEpic';

export class ClientDependencyContainer extends DependencyContainer {

  public store() {
    return this.storeFactory().create();
  }

  public socketMessageService() {
    return new ChatSocketService(
      this.store(),
      this.serializer(),
      process.env.env !== 'production' ? '4000' : undefined,
    );
  }

  public logger(): LoggerInterface {
    return new ConsoleLogger();
  }

  public serializer() {
    return this.serializerFactory().create();
  }

  private storeFactory() {
    return new StoreFactory([
      this.epics(),
      multipleMiddleware,
      promiseMiddleware,
      storeMiddleware,
      ignoreMiddleware,
      chatSocketMiddleware(this.socketMessageService),
    ]);
  }

  private epics(): Middleware {
    return createEpicMiddleware(
      combineEpics(
        timerEpic,
        raptorEpic,
        wowEpic,
        clippyEpic,
      ),
    );
  }

  private serializerFactory() {
    return new SerializerFactory();
  }

}
