import * as socketIoClient from 'socket.io-client';
import { ClientStore } from '../ClientStore';
import { socketConnected, socketDisconnected } from '../Action/app';
import { SerializerInterface } from '../../shared/Serializer/SerializerInterface';
import { receiveInitialStateFromServer, receiveMessageFromServer } from '../Action/chat';

/**
 * Socket connection for message namespace.
 */
export class ChatSocketService {
  private socket;

  constructor(
    private store: ClientStore,
    private serializer: SerializerInterface,
    private port?: string) {

  }

  public connect() {
    this.socket = socketIoClient(`/chat`, { port: this.port });
    this.socket.on('connect', () => {
      this.store.dispatch(socketConnected());
    });
    this.socket.on('disconnect', () => {
      this.store.dispatch(socketDisconnected());
    });
    this.socket.on('message', (serializedData) => {
      const message = this.serializer.deserialize(serializedData);
      this.store.dispatch(receiveMessageFromServer(message));
    });
    this.socket.on('initialState', (serializedData) => {
      const state = this.serializer.deserialize(serializedData);
      this.store.dispatch(receiveInitialStateFromServer(state));
    });
  }

  public sendMessage(message: string) {
    this.socket.emit('message', message);
  }

}
