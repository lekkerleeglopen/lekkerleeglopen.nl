import { ClientStore } from '../ClientStore';
import { ClientState } from '../State/ClientState';

function isFunction(action: any): action is (state: ClientState, store: ClientStore) => any {
  return typeof action === 'function';
}

export function storeMiddleware(store) {
  return (next) => (action) => {
    if (isFunction(action)) {
      return store.dispatch(action(store.getState(), store));
    }
    return next(action);
  };
}
