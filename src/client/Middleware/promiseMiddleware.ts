
function isPromise(action: any): action is Promise<any> {
  return typeof action === 'object' &&
    action !== null &&
    typeof action.then === 'function' &&
    typeof action.catch === 'function';
}

export function promiseMiddleware(store) {
  return (next) => (action) => {
    if (isPromise(action)) {
      return action.then((newAction) => {
        store.dispatch(newAction);
      });
    }
    return next(action);
  };
}
