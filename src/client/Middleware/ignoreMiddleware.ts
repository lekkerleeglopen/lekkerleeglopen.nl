
function isFalse(action: any): action is false {
  return action === false;
}

export function ignoreMiddleware(store) {
  return (next) => (action) => {
    if (isFalse(action)) {
      return;
    }
    return next(action);
  };
}
