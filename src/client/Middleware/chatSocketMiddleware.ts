import { ChatSocketService } from '../Socket/ChatSocketService';
import { CHAT_SEND_MESSAGE_TO_SERVER } from '../Action/chat';

function isSocketMessage(action: any): action is {type: string, message: string} {
  return typeof action === 'object' && action.type  === CHAT_SEND_MESSAGE_TO_SERVER;
}

export function chatSocketMiddleware(socketService: () => ChatSocketService) {
  return (store) => (next) => (action) => {
    if (isSocketMessage(action)) {
      socketService().sendMessage(action.message);
    }
    return next(action);
  };
}
