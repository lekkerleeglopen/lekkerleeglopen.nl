
function isArray(action: any): action is any[] {
  return action instanceof Array;
}

export function multipleMiddleware(store) {
  return (next) => (action) => {
    if (isArray(action)) {
      return action.forEach((ac) => {
        store.dispatch(ac);
      });
    }
    return next(action);
  };
}
