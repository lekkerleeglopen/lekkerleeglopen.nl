import 'jest';
import { promiseMiddleware } from '../promiseMiddleware';

describe('promiseMiddleware', () => {

  it('Should ignore none promise actions', () => {
    const next = jest.fn();
    let action: any = {
      type: 'not a promise',
    };
    const store: any = {
      next,
    };
    promiseMiddleware(store as any)(next)(action);
    expect(next).toBeCalledWith(action);

    action = {
      command: {},
    };
    promiseMiddleware(store as any)(next)(action);
    expect(next).toBeCalledWith(action);

    promiseMiddleware(store as any)(next)(null);
    expect(next).toBeCalledWith(null);
  });

  it('Should resolve promise actions', async () => {
    const dispatch = jest.fn();
    const action: any = {
      type: 'The action',
    };
    const store: any = {
      dispatch,
    };
    await promiseMiddleware(store as any)(null)(Promise.resolve(action));
    expect(dispatch).toBeCalledWith(action);
  });

});
