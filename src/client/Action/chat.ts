import { ClientState } from '../State/ClientState';
import { Message } from '../State/Message';

export const CHAT_SEND_MESSAGE_TO_SERVER = 'CHAT_SEND_MESSAGE_TO_SERVER';
export const CHAT_RECEIVED_MESSAGE_FROM_SERVER = 'CHAT_RECEIVED_MESSAGE_FROM_SERVER';
export const CHAT_RECEIVED_INITIAL_STATE_FROM_SERVER = 'CHAT_RECEIVED_INITIAL_STATE_FROM_SERVER';
export const CHAT_REMOVE_OLD_MESSAGES = 'CHAT_REMOVE_OLD_MESSAGES';

export function sendMessageToServer() {
  return (state: ClientState) => {
    if (state.app.message.trim() === '') {
      return false;
    }
    return {
      type: CHAT_SEND_MESSAGE_TO_SERVER,
      message: state.app.message,
    };
  };
}

export function receiveMessageFromServer(message: Message) {
  return {
    type: CHAT_RECEIVED_MESSAGE_FROM_SERVER,
    message,
  };
}

export function receiveInitialStateFromServer(state) {
  return {
    type: CHAT_RECEIVED_INITIAL_STATE_FROM_SERVER,
    state,
  };
}

export function chatRemoveOldMessages() {
  return {
    type: CHAT_REMOVE_OLD_MESSAGES,
    date: new Date(),
  };
}
