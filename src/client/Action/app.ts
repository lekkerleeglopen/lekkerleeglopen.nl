export const APP_SOCKET_CONNECTED = 'APP_SOCKET_CONNECTED';
export const APP_SOCKET_DISCONNECTED = 'APP_SOCKET_DISCONNECTED';
export const APP_USER_UPDATES_PENDING_MESSAGE = 'APP_USER_UPDATES_PENDING_MESSAGE';

export function socketConnected() {
  return {
    type: APP_SOCKET_CONNECTED,
  };
}

export function socketDisconnected() {
  return {
    type: APP_SOCKET_DISCONNECTED,
  };
}

export function updateMessage(message: string) {
  return {
    type: APP_USER_UPDATES_PENDING_MESSAGE,
    message,
  };
}
