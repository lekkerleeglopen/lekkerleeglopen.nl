import { applyMiddleware, combineReducers, createStore, Middleware, Reducer } from 'redux';

import { appReducer } from './Reducer/appReducer';
import { chatReducer } from './Reducer/chatReducer';
import { ClientState } from './State/ClientState';
import { ClientStore } from './ClientStore';
import { AppState } from './State/AppState';
import { ChatState } from './State/ChatState';
import { composeWithDevTools } from 'redux-devtools-extension';

export class StoreFactory {

  constructor(private middleware: Middleware[]) {

  }

  public create(): ClientStore {
    const reducers: Reducer<ClientState> = combineReducers<ClientState>({
      app: appReducer,
      chat: chatReducer,
    });
    const defaultState: ClientState = {
      app: new AppState(),
      chat: new ChatState(),
    };
    const composeEnhancers = composeWithDevTools({
      name: 'application',
    });
    return createStore<ClientState>(reducers as any, defaultState, composeEnhancers(applyMiddleware(...this.middleware)));
  }

}
