
import { SerializerInterface } from '../shared/Serializer/SerializerInterface';
import { Serializer } from '../shared/Serializer/Serializer';
import { TaggedText } from './State/TaggedText';
import { ChatState } from './State/ChatState';
import { Message } from './State/Message';

export class SerializerFactory {

  public create(): SerializerInterface {
    return new Serializer([
      TaggedText,
      Message,
      ChatState,
    ]);
  }

}
