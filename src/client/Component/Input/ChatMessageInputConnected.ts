import { updateMessage } from '../../Action/app';
import { sendMessageToServer } from '../../Action/chat';
import { connect } from 'react-redux';
import { ClientState } from '../../State/ClientState';
import { ChatMessageInput } from './ChatMessageInput';

const mapStateToProps = (state: ClientState) => {
  return {
    message: state.app.message,
  };
};

const matchDispatchToProps = {
  sendMessage: sendMessageToServer,
  updateMessage,
};

export const ChatMessageInputConnected = connect(mapStateToProps, matchDispatchToProps)(ChatMessageInput);
