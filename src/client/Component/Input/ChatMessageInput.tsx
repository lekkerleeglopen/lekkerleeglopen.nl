/**
 * Component.
 */

import * as React from 'react';
import { sendMessageToServer } from '../../Action/chat';
import { updateMessage } from '../../Action/app';

export class ChatMessageInput extends React.PureComponent<{
  sendMessage: typeof sendMessageToServer,
  updateMessage: typeof updateMessage,
  message: string;
}, {}> {

  public render(): React.ReactNode {
    const { message } = this.props;
    return (
      <form className="input-box" onSubmit={this.submit}>
        <input placeholder="Wat wil je kwijt?" className="input-box__text" value={message} autoFocus={true} onChange={this.updateMessage}/>
        <button className="input-box__button" type="submit">&#x279C;</button>
      </form>
    );
  }

  private updateMessage = (e) => {
    this.props.updateMessage(e.target.value);
  };

  private submit = (event) => {
    this.props.sendMessage();
    event.preventDefault();
  };
}
