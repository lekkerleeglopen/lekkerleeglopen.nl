/**
 * Component.
 */

import * as React from 'react';
import { TaggedText } from '../../State/TaggedText';
import { TextDecorator } from '../Decorater/TextDecorator';
import { Message } from '../../State/Message';

export class ChatFeedMessageText extends React.PureComponent<{
  text: TaggedText,
  message: Message,
}, {}> {

  public render(): React.ReactNode {
    const { text, message } = this.props;
    const parts = text.parts.map((value, index) => {
      if (typeof value === 'string') {
        return <span key={index}>{value}</span>;
      }
      return <ChatFeedMessageText message={message} key={index} text={value}/>
    });
    return (
      <TextDecorator message={message} text={text}>
        {parts}
      </TextDecorator>
    );
  }
}
