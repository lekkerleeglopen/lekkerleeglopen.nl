import { connect } from 'react-redux';
import { ChatFeed } from './ChatFeed';
import { ClientState } from '../../State/ClientState';

const mapStateToProps = (state: ClientState) => {
  return {
    messages: state.chat.messages,
  };
};

export const ChatFeedConnected = connect(mapStateToProps)(ChatFeed);
