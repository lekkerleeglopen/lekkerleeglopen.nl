/**
 * Component.
 */

import * as React from 'react';
import { List } from 'immutable';
import { Message } from '../../State/Message';
import { ChatFeedMessage } from './ChatFeedMessage';

export class ChatFeed extends React.PureComponent<{
  messages: List<Message>,
}, {}> {

  public render(): React.ReactNode {
    const { messages } = this.props;
    const components = messages.map((message) => {
      return <ChatFeedMessage message={message} key={message.uuid}/>;
    });
    return (
      <div className="feed">
        {components}
      </div>
    );
  }
}
