/**
 * Component.
 */

import * as React from 'react';
import { Message } from '../../State/Message';
import { NLTimeAgo } from '../NLTimeAgo';
import { MessageDecorator } from '../Decorater/MessageDecorator';
import { ChatFeedMessageText } from './ChatFeedMessageText';

export class ChatFeedMessage extends React.PureComponent<{
  message: Message,
}, {}> {
  public render(): React.ReactNode {
    const { message } = this.props;
    return (
      <MessageDecorator message={message}>
        <div className="message">
          <div className="message__text">
            <ChatFeedMessageText message={message} text={message.text}/>
          </div>
          <div className="message__time">
            <NLTimeAgo date={message.postedOn}/>
          </div>
        </div>
      </MessageDecorator>

    );
  }
}
