import * as React from 'react';
import { createMessageDecoratorsChain } from './createMessageDecoratorsChain';
import { Message } from '../../State/Message';
import { ReactNode } from 'react';
import { StackTraceMessageDecorator } from './Message/StackTraceMessageDecorator';
import { ChatFeedMessageText } from '../Feed/ChatFeedMessageText';

const decorate = createMessageDecoratorsChain(
  StackTraceMessageDecorator,
);

export class MessageDecorator extends React.PureComponent<{
  message: Message,
  children: ReactNode,
}, {}> {
  public render(): React.ReactNode {
    const { message } = this.props;
    const textRenderer = (text) => <ChatFeedMessageText message={message} text={text}/>;
    return decorate(textRenderer)(this.undecoratedMessage)(message);
  }

  private undecoratedMessage = () => this.props.children;
}
