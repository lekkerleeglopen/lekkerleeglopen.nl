import { TaggedText } from '../../State/TaggedText';
import { ReactNode } from 'react';
import { Message } from '../../State/Message';

export type TextRenderer = (text: TaggedText) => ReactNode;

export type TextDecorator = (next: TextRenderer) => TextRenderer;

export type TextDecoratorChain = (message: Message) => TextDecorator;

/**
 * This will chain all decorators to each other.
 */
export function createTextDecoratorsChain(...decorators: TextDecoratorChain[]): TextDecoratorChain {
  return (message: Message) => (first: TextRenderer) => {
    return decorators.reverse().reduce<TextRenderer>((prev: TextRenderer, next: TextDecoratorChain) => {
      return (text) => next(message)(prev)(text);
    },                                               first);
  };
}
