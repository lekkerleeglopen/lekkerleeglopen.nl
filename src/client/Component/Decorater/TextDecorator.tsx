import { TaggedText } from '../../State/TaggedText';
import { createTextDecoratorsChain } from './createTextDecoratorsChain';
import * as React from 'react';
import { ProgrammingTextDecorator } from './Text/ProgrammingTextDecorator';
import { ReactNode } from 'react';
import { ProfanityTextDecorator } from './Text/ProfanityTextDecorator';
import { ToImageDecorator } from './Text/ToImageDecorator';
import { UppercaseTextDecorator } from './Text/UppercaseTextDecorator';
import { KittenDecorator } from './Text/KittenDecorator';
import { BugTextDecorator } from './Text/BugTextDecorator';
import { MemeTextDecorator } from './Text/MemeTextDecorator';
import { Message } from '../../State/Message';
import { ClippyDecorator } from './Text/ClippyDecorator';
import { OembedTextDecorator } from './Text/OembedDecorator';

const decorate = createTextDecoratorsChain(
  ProgrammingTextDecorator,
  OembedTextDecorator,
  ProfanityTextDecorator,
  BugTextDecorator,
  UppercaseTextDecorator,
  KittenDecorator,
  MemeTextDecorator,
  ClippyDecorator,
  ToImageDecorator,
);

export class TextDecorator extends React.PureComponent<{
  text: TaggedText,
  message: Message,
  children: ReactNode,
}, {}> {

  public render(): React.ReactNode {
    return decorate(this.props.message)(this.rootChild)(this.props.text);
  }
  private rootChild = () => this.props.children;
}
