import 'jest';
import * as React from 'react';
import { createMessageDecoratorsChain, MessageDecoratorChain } from '../createMessageDecoratorsChain';
import { ReactNode } from 'react';
import { Message } from '../../../State/Message';
import { TextRenderer } from '../createTextDecoratorsChain';

/* tslint:disable:no-shadowed-variable */
describe('createMessageDecoratorsChain', () => {

  it('Should pass with single item', () => {
    const first: MessageDecoratorChain = (textRenderer) => (next) => (message) => {
      return [textRenderer, message] as ReactNode;
    };
    const chain = createMessageDecoratorsChain(
      first,
    );
    const message = new Message();
    const initialRenderer = jest.fn();
    const textRender: TextRenderer = jest.fn();
    expect(chain(textRender)(initialRenderer)(message)).toEqual([textRender, message]);
    expect(initialRenderer).not.toBeCalled();
  });

  it('Should be able to pass to initial renderer', () => {
    const first: MessageDecoratorChain = (textRenderer) => (next) => (message) => {
      return next(message);
    };
    const chain = createMessageDecoratorsChain(
      first,
    );
    const message = new Message();
    const initialRenderer = (message) => {
      return message;
    };
    const textRender: TextRenderer = jest.fn();
    expect(chain(textRender)(initialRenderer)(message)).toEqual(message);
  });

  it('Should pass with second item', () => {
    const first: MessageDecoratorChain = (textRenderer) => (next) => (message) => {
      return next(message);
    };
    const second: MessageDecoratorChain = (textRenderer) => (next) => (message) => {
      return [textRenderer, message, 2] as ReactNode;
    };
    const chain = createMessageDecoratorsChain(
      first,
      second,
    );
    const message = new Message();
    const initialRenderer = jest.fn();
    const textRender: TextRenderer = jest.fn();
    expect(chain(textRender)(initialRenderer)(message)).toEqual([textRender, message, 2]);
    expect(initialRenderer).not.toBeCalled();
  });

  it('Should be able to fallback to default', () => {
    const first: MessageDecoratorChain = (textRenderer) => (next) => (message) => {
      return next(message);
    };
    const second: MessageDecoratorChain = (textRenderer) => (next) => (message) => {
      return next(message);
    };
    const chain = createMessageDecoratorsChain(
      first,
      second,
    );
    const message = new Message();
    const initialRenderer = (message) => {
      return message;
    };
    const textRender: TextRenderer = jest.fn();
    expect(chain(textRender)(initialRenderer)(message)).toEqual(message);
  });

});
