import 'jest';
import * as React from 'react';
import { createTextDecoratorsChain } from '../createTextDecoratorsChain';
import { ReactNode } from 'react';
import { Message } from '../../../State/Message';
import { TaggedText } from '../../../State/TaggedText';
import { List } from 'immutable';

/* tslint:disable:no-shadowed-variable */
describe('createTextDecoratorsChain', () => {

  it('Should pass with single item', () => {
    const first = (message) => (next) => (text) => {
      return [message, text] as ReactNode;
    };
    const chain = createTextDecoratorsChain(
      first,
    );
    const message = new Message();
    const text = new TaggedText();
    const initialRenderer = (text) => {
      return <p>Nothing</p>;
    };
    expect(chain(message)(initialRenderer)(text)).toEqual([message, text]);
  });

  it('Should be able to pass to initial renderer', () => {
    const first = (message) => (next) => (text) => {
      return next(text);
    };
    const chain = createTextDecoratorsChain(
      first,
    );
    const message = new Message();
    const text = new TaggedText();
    const initialRenderer = (text) => {
      return text;
    };
    expect(chain(message)(initialRenderer)(text)).toBe(text);
  });

  it('Should be able to pass to pass different text to initial renderer', () => {
    const text2 = new TaggedText({ parts: List(['hi']) });
    const first = (message) => (next) => (text) => {
      return next(text2);
    };
    const chain = createTextDecoratorsChain(
      first,
    );
    const message = new Message();
    const text = new TaggedText();
    const initialRenderer = (text) => {
      return text;
    };
    expect(chain(message)(initialRenderer)(text)).toBe(text2);
  });

  it('Should pass with second item', () => {
    const first = (message) => (next) => (text) => {
      return [next(text), 1];
    };
    const second = (message) => (next) => (text) => {
      return [message, text, 2] as ReactNode;
    };
    const chain = createTextDecoratorsChain(
      first,
      second,
    );
    const message = new Message();
    const text = new TaggedText();
    const initialRenderer = (text) => {
      return <p>Nothing</p>;
    };
    expect(chain(message)(initialRenderer)(text)).toEqual([[message, text, 2], 1]);
  });

  it('Should be able to fallback to default', () => {
    const first = (message) => (next) => (text) => {
      return next(text);
    };
    const second = (message) => (next) => (text) => {
      return next(text)
    };
    const chain = createTextDecoratorsChain(
      first,
      second,
    );
    const message = new Message();
    const text = new TaggedText();
    const initialRenderer = (text) => {
      return text;
    };
    expect(chain(message)(initialRenderer)(text)).toEqual(text);
  });

});
