import * as React from 'react';
import { TextDecoratorChain } from '../createTextDecoratorsChain';
import { TEXT_TAG_UPPERCASE } from '../../../../server/Dependency/Tags';

export const UppercaseTextDecorator: TextDecoratorChain = () => (next) => (text) => {
  if (text.tags.contains(TEXT_TAG_UPPERCASE)) {
    const random = text.randomBetween('-', 30);
    return <span className={`tag--uppercase tag--uppercase-v${random}`}>{next(text)}</span>;
  }
  return next(text);
};
