import * as React from 'react';
import { TEXT_TAG_OEMBED } from '../../../../server/Dependency/Tags';
import { TextDecoratorChain } from '../createTextDecoratorsChain';

interface OembedData {
  html: string,
  width: number,
  author_url: string,
  thumbnail_height: number,
  height: number,
  provider_name: string,
  title: string,
  author_name: string,
  provider_url: string,
  version: string,
  type: string,
  thumbnail_url: string
}

export class Oembed extends React.PureComponent<{
  oembed: OembedData,
  url: string,
}, {}> {

  public render(): React.ReactNode {
    const { oembed, url } = this.props;
    if (typeof oembed === 'undefined' || typeof oembed.html !== 'string') {
      return <a href={url} target="_blank">{url}</a>;
    }
    return <div className="tag--oembed tag--oembed-container" dangerouslySetInnerHTML={this.createMarkup()}/>;
  }

  private createMarkup = () => {
    return { __html: this.props.oembed.html };
  };
}

export const OembedTextDecorator: TextDecoratorChain = () => (next) => (text) => {
  if (text.tags.contains(TEXT_TAG_OEMBED)) {
    return <Oembed url={text.toString()} oembed={text.metadata.get('oembed')}/>;
  }
  return next(text);
};
