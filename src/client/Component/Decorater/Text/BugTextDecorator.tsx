import * as React from 'react';
import { ReactNode } from 'react';
import { TEXT_TAG_BUG } from '../../../../server/Dependency/Tags';
import { TextDecoratorChain } from '../createTextDecoratorsChain';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import { of } from 'rxjs/observable/of';
import { Subscription } from 'rxjs/Subscription';

declare var BugController: any;
declare var SpiderController: any;

export class DrawBugs extends React.PureComponent<{
  children: ReactNode,
  lifetimeLeft: number,
}, {}> {

  private flyController;
  private spiderController;
  private subscription: Subscription;

  public componentDidMount(): void {
    if (this.props.lifetimeLeft === 0) {
      return;
    }
    this.subscription = of(1)
      .delay(this.props.lifetimeLeft)
      .do(this.exterminate.bind(this))
      .delay(3000)
      .subscribe(this.componentWillUnmount.bind(this));
    this.flyController = new BugController({
      minBugs: 1,
      maxBugs: 4,
      imageSprite: '/public/bug/fly-sprite.png',
    });
    this.spiderController = new SpiderController({
      minBugs: 1,
      maxBugs: 4,
      imageSprite: '/public/bug/spider-sprite.png',
    });
  }

  public exterminate() {
    this.flyController.killAll();
    this.spiderController.killAll();
  }

  public componentWillUnmount(): void {
    if (this.props.lifetimeLeft === 0) {
      return;
    }
    this.flyController.end();
    this.spiderController.end();
    this.subscription.unsubscribe();
  }

  public render(): React.ReactNode {
    return this.props.children;
  }
}

export const BugTextDecorator: TextDecoratorChain = (message) => (next) => (text) => {
  if (text.tags.contains(TEXT_TAG_BUG)) {
    return <DrawBugs lifetimeLeft={message.timeLeftOfTimer(60 * 1000)}>{next(text)}</DrawBugs>;
  }
  return next(text);
};
