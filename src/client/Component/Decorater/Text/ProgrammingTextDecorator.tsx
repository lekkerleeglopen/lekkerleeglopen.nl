import * as React from 'react';
import { TextDecoratorChain } from '../createTextDecoratorsChain';
import { TEXT_TAG_PROGRAMMING } from '../../../../server/Dependency/Tags';

export const ProgrammingTextDecorator: TextDecoratorChain = () => (next) => (text) => {
  if (text.tags.contains(TEXT_TAG_PROGRAMMING)) {
    return <span className="tag--programming">{next(text)}</span>;
  }
  return next(text);
};
