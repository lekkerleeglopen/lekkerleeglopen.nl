import * as React from 'react';
import { ReactNode } from 'react';
import { TEXT_TAG_MEME } from '../../../../server/Dependency/Tags';
import { TextDecoratorChain } from '../createTextDecoratorsChain';

export class DrawCanvas extends React.PureComponent<{
  children: ReactNode,
  message: string,
  canvasId: string,
}, {}> {

  public render(): React.ReactNode {
    return this.props.children;
  }

  public componentDidMount(): void {
    const canvas = document.getElementById(this.props.canvasId) as HTMLCanvasElement;
    if (canvas.getContext) {
      const ctx = canvas.getContext('2d');
      const img1 = new Image();
      img1.src = 'public/textToImages/The-Scroll-Of-Truth.jpg';

      img1.onload = () => {
        ctx.drawImage(
          img1,
          0,
          0,
          img1.width,
          img1.height,     // source rectangle
          0,
          0,
          canvas.width,
          canvas.height,
        );

        this.wrapText(ctx, this.props.message, 170, 450, 150, 25);
      };
    }
  }

  private wrapText(context, text, x, y, maxWidth, lineHeight): void {
    const words = text.split(' ');
    let line = '';
    const localX = x;
    let localY = y;
    context.fillStyle = 'black';
    context.font = '42px "Peterbuilt"';

    for (let n = 0; n < words.length; n += 1) {
      const testLine = line + words[n] + ' ';
      const metrics = context.measureText(testLine);
      const testWidth = metrics.width;
      if (testWidth > maxWidth && n > 0) {
        context.fillText(line, localX, localY);
        line = words[n] + ' ';
        localY += lineHeight;
      } else {
        line = testLine;
      }
    }
    context.fillText(line, localX, localY);
  }

}

export const MemeTextDecorator: TextDecoratorChain = (message) => (next) => (text) => {
  if (text.tags.contains(TEXT_TAG_MEME)) {
    const rnd = 'canvas' + (Math.random() * 999999);
    return (
      <DrawCanvas canvasId={rnd} message={message.text.toString()}>
        <canvas id={rnd} height="728" width="885" className="canvas-scroll"/>
      </DrawCanvas>
    );
  }
  return next(text);
};
