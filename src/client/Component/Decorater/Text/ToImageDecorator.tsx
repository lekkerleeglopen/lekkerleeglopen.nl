import * as React from 'react';
import { TextDecoratorChain } from '../createTextDecoratorsChain';
import { TEXT_TAG_TO_IMAGE } from '../../../../server/Dependency/Tags';

export const ToImageDecorator: TextDecoratorChain = () => (next) => (text) => {
  if (text.tags.contains(TEXT_TAG_TO_IMAGE) && text.metadata.has('img')) {
    return <img className="tag--text-to-img" srcSet={text.metadata.get('img')} alt={text.toString()} />;
  }
  return next(text);
};
