import * as React from 'react';
import { TextDecoratorChain } from '../createTextDecoratorsChain';
import { TEXT_TAG_PROFANITY } from '../../../../server/Dependency/Tags';
import * as classNames from 'classnames';

export const ProfanityTextDecorator: TextDecoratorChain = () => (next) => (text) => {
  if (text.tags.contains(TEXT_TAG_PROFANITY)) {
    const random = text.randomBetween('-', 20);
    const classes = classNames({
      'tag--profanity': true,
      'tag--profanity-red': random === 0,
      'tag--profanity-darkred': random === 1,
      'tag--profanity-wordart-blue': random === 2,
      'tag--profanity-wordart-rainbow': random === 3,
      'tag--profanity-rainbow-drawn': random === 4,
      'tag--profanity-wordart-yellow': random === 5,
    });
    return <span className={classes}>{next(text)}</span>;
  }
  return next(text);
};
