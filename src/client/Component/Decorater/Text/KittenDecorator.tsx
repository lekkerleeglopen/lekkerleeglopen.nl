import * as React from 'react';
import { TEXT_TAG_KITTEN } from '../../../../server/Dependency/Tags';
import { TextDecoratorChain } from '../createTextDecoratorsChain';

export const KittenDecorator: TextDecoratorChain = () => (next) => (text) => {
  if (text.tags.contains(TEXT_TAG_KITTEN) && text.metadata.has('img')) {
    return <img className="tag--kitten" src={text.metadata.get('img')} alt={text.toString()} />;
  }
  return next(text);
};
