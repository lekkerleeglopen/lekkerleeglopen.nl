import * as React from 'react';
import { ReactNode } from 'react';
import {
  TEXT_TAG_CLIPPY,
} from '../../../../server/Dependency/Tags';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/do';
import { of } from 'rxjs/observable/of';
import { Subscription } from 'rxjs/Subscription';
import { TextDecoratorChain } from '../createTextDecoratorsChain';

declare var clippy: any;
declare var clippies: Clippy[];

export class Clippy extends React.PureComponent<{
  children: ReactNode,
  lifetimeSeconds: number,
  agent: string,
  x: number,
  y: number,
}, {}> {

  private subscription: Subscription;
  private clippy;

  public componentDidMount(): void {
    if (this.props.lifetimeSeconds === 0) {
      return;
    }
    const { x, y } = this.props;

    this.subscription = of(1)
      .delay(this.props.lifetimeSeconds)
      .do(this.removeClippy.bind(this))
      .delay(2000)
      .subscribe(this.componentWillUnmount.bind(this));
    clippy.load(
      this.props.agent,
      (agent) => {
        clippies.push(this);
        this.clippy = agent;
        agent.moveTo(
          x,
          y,
        );
        // Do anything with the loaded agent
        agent.show();
      },
      (error) => {
        throw new Error(error);
      },
    );
  }

  public speak(message: string) {
    this.clippy.speak(message);
  }

  public animate(message: string) {
    this.clippy.play(message);
  }

  public removeClippy() {
    if (!this.clippy) {
      return;
    }
    clippies.splice(clippies.indexOf(this), 1);
    this.clippy.hide();
  }

  public componentWillUnmount() {
    if (!this.clippy) {
      return;
    }
    this.clippy.stop();
  }

  public render(): React.ReactNode {
    return this.props.children;
  }

}

export const ClippyDecorator: TextDecoratorChain = (message) => (next) => (text) => {
  if (text.tags.contains(TEXT_TAG_CLIPPY)) {
    return (
      <Clippy
        x={text.randomBetween('x', window.innerWidth - 200)}
        y={text.randomBetween('y', window.innerHeight - 200)}
        agent={text.metadata.get('agent')}
        lifetimeSeconds={message.timeLeftOfTimer(60 * 1000)}
      >
        {next(text)}
      </Clippy>);
  }
  return next(text);
};
