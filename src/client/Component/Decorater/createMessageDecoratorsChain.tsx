import * as React from 'react';
import { ReactNode } from 'react';
import { Message } from '../../State/Message';
import { TextRenderer } from './createTextDecoratorsChain';

export type MessageRenderer = (message: Message) => ReactNode;

export type MessageDecorator = (next: MessageRenderer) => MessageRenderer;

export type MessageDecoratorChain = (textRenderer: TextRenderer) => MessageDecorator;

/**
 * This will chain all decorators to each other.
 */
export function createMessageDecoratorsChain(...decorators: MessageDecoratorChain[]): MessageDecoratorChain {
  return (textRenderer: TextRenderer) => (first: MessageRenderer) => {
    return decorators.reverse().reduce<MessageRenderer>((prev: MessageRenderer, next: MessageDecoratorChain) => {
      return (message) => next(textRenderer)(prev)(message);
    },                                                  first);
  };
}
