import * as React from 'react';
import { MessageDecoratorChain } from '../createMessageDecoratorsChain';
import { MESSAGE_BLUE_SCREEN_OF_DEATH, MESSAGE_SERVER_ERROR } from '../../../../server/Dependency/Tags';
import * as classNames from 'classnames';
import { NLTimeAgo } from '../../NLTimeAgo';

const StackTrace = ({ message, renderText }) => {

  const items = message.text.parts.map((text, index) => {
    const classes = classNames({
      'stack-trace__error': index === 0,
    });
    const rendered = typeof text === 'string' ? text : renderText(text);
    return (
      <li className={classes} key={index}>
        {rendered}
      </li>
    );
  });

  let cause = null;
  if (message.metadata.has('textMessage')) {
    cause = (
      <span className="stack-trace__cause">
          <p>The following message:</p>
          {message.metadata.get('textMessage')}
          <p>Triggered the following error:</p>
     </span>
    );
  }
  return (
    <div className="stack-trace">
      <header>
        <h1 className="stack-trace__emotion">:(</h1>
      </header>
      <div className="stack-trace__description">
        Your PC ran into a problem and needs to restart. We're just collecting some error
        info, and then we'll restart for you.<span role="progress">&nbsp;(<NLTimeAgo date={message.postedOn}/>)</span>
        {cause}
        <ul className="stack-trace__trace">
          {items}
        </ul>
      </div>
      <footer>
        <p>
          <small>If you'd like to know more, you can search online later for this
            error:
            <span>
                <a
                  href="https://support.microsoft.com/nl-nl/help/14238/windows-10-troubleshoot-blue-screen-errors"
                >
                  &nbsp;HAL_INITIALIZATION_FAILED
                </a>
              </span>
          </small>
        </p>
      </footer>
    </div>

  );
};

export const StackTraceMessageDecorator: MessageDecoratorChain = (renderText) => (next) => (message) => {
  if (message.tags.contains(MESSAGE_SERVER_ERROR) || message.tags.contains(MESSAGE_BLUE_SCREEN_OF_DEATH)) {
    return <StackTrace message={message} renderText={renderText}/>
  }
  return next(message);
};
