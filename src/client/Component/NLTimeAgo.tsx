import * as React from 'react';

import { default as TimeAgo } from 'react-timeago';
import nl from 'react-timeago/lib/language-strings/nl';
import buildFormatter from 'react-timeago/lib/formatters/buildFormatter';

const formatter = buildFormatter(nl);

export const NLTimeAgo = (props) => {
  return <TimeAgo {...props} formatter={formatter}/>
};
