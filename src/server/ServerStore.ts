
import { Store } from 'redux';
import { ServerState } from './State/ServerState';

export interface ServerStore extends Store<ServerState> {}
