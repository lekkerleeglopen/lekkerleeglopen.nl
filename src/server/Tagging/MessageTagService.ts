import { MessageVisitor } from './MessageVisitor';

export interface MessageTagService {

  tagMessage(tag: MessageVisitor): Promise<void> | void;

}
