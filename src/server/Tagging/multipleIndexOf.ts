
export function multipleIndexOf(text: string, needle: string): number[] {
  if (needle === '') {
    return [];
  }
  const found = [];
  const textLength = needle.length;
  let offset = 0;
  let haystack = text;
  let index = haystack.indexOf(needle);
  while (index >= 0) {
    found.push(offset + index);
    const endOfCurrentNeedle = index + textLength;
    offset += endOfCurrentNeedle;
    haystack = haystack.slice(endOfCurrentNeedle);
    index = haystack.indexOf(needle)
  }
  return found;
}
