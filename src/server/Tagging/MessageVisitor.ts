import { Set, Map } from 'immutable';

/**
 * TODO: rename
 */
export class MessageVisitor {

  public readonly messageToLowerCase;
  private tags: Set<string> = Set();
  private metadata: Map<string, any> = Map();

  constructor(public readonly message: string) {
    this.messageToLowerCase = message.toLowerCase();
  }

  public addTags(tags: string[], metadata: {[key: string]: any} = {}): this {
    this.tags = this.tags.merge(tags);
    this.metadata = this.metadata.merge(metadata);
    return this;
  }

  public getTags(): Set<string> {
    return this.tags;
  }

  public getMetadata(): Map<string, any> {
    return this.metadata;
  }
}
