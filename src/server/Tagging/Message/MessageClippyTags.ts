import { MessageTagService } from '../MessageTagService';
import { MessageVisitor } from '../MessageVisitor';
import * as fs from 'fs';
import * as path from 'path';
import { TEXT_TAG_CLIPPY, MESSAGE_TAG_CLIPPY_ANIMATE } from '../../Dependency/Tags';
import { TextTagService } from '../TextTagService';
import { TaggedTextBuilder } from '../TaggedTextBuilder';
import { Component } from '@nestjs/common';
import { TranslateService } from '../../Translate/TranslateService';
import { FileCachedTranslateService } from '../../Translate/FileCachedTranslateService';

const clippyDirectory = path.join(__dirname, '..', '..', '..', '..', '/public/clippy');

@Component()
export class MessageClippyTags implements MessageTagService, TextTagService {

  private agentsActions: { [action: string]: string[] } = {};
  private agents: string[] = [];
  private cachedTranslateService: FileCachedTranslateService;

  constructor(translate: TranslateService) {
    this.cachedTranslateService = new FileCachedTranslateService(translate, path.join(clippyDirectory, 'translate.json'));

  }

  public tagMessage(tag: MessageVisitor): void {
    Object.getOwnPropertyNames(this.agentsActions).forEach((animation) => {
      if (tag.messageToLowerCase.indexOf(animation) >= 0) {
        const options = this.agentsActions[animation];
        const nr = Math.floor(Math.random() * options.length);
        tag.addTags([MESSAGE_TAG_CLIPPY_ANIMATE], { animation: options[nr] });
      }
    });
  }

  public tagText(builder: TaggedTextBuilder) {
    this.agents.forEach((agent) => {
      const indexOf = builder.textToLowerCase.indexOf(agent.toLowerCase());
      if (indexOf >= 0) {
        builder.tagSlice(indexOf, agent.length, [TEXT_TAG_CLIPPY], { agent });
      }
    });
  }

  public async load(): Promise<void> {
    const agents = fs.readdirSync(path.join(clippyDirectory, 'agents'));
    for (const name of agents) {
      this.agents.push(name);
      await new Promise((resolve, reject) => {
        (global as any).clippy = {
          ready: (agent, options) => {
            const animations = Object.getOwnPropertyNames(options.animations);
            Promise.all(animations.map(async (animation) => {
              // Convert camel casing and remove any special characters
              const m = /([a-zA-Z]*)/g.exec(animation);
              const text = m[0].replace(/([a-z])([A-Z])/g, '$1 $2').toLowerCase();
              if (!this.agentsActions[text]) {
                this.agentsActions[text] = [];
              }
              this.agentsActions[text].push(animation);
              const dutch = (await this.cachedTranslateService.translateEnglishToDutch(text)).toLowerCase();
              if (!this.agentsActions[dutch]) {
                this.agentsActions[dutch] = [];
              }
              this.agentsActions[dutch].push(animation);
            })).then(resolve).catch(reject);
          },
        };
        require(path.join(clippyDirectory, 'agents', name, 'agent.js'));
      });
    }
  }

}
