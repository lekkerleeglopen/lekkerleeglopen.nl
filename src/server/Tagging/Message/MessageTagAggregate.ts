import { MessageTagService } from '../MessageTagService';
import { MessageVisitor } from '../MessageVisitor';
import { className } from '../../className';

export class MessageTagAggregate implements MessageTagService {

  constructor(private services: MessageTagService[]) {
    services.forEach((service) => {
      if (typeof service.tagMessage !== 'function') {
        throw new Error(`Service "${className(service)}" should implement tagMessage`);
      }
    });
  }

  public tagMessage(tag: MessageVisitor): Promise<void> {
    return Promise.all(this.services.map((service) => {
      return Promise.resolve(service.tagMessage(tag));
    })) as Promise<any>;
  }

}
