import { TagMessageByTexts } from './TagMessageByTexts';
import { MESSAGE_BLUE_SCREEN_OF_DEATH } from '../../Dependency/Tags';

export const MessageStackTraceTag = new TagMessageByTexts([
  'exception',
  'bsod',
  'blue screen of death',
  'blue screen',
  'fatal error',
  'stack trace',
],                                                        [
  MESSAGE_BLUE_SCREEN_OF_DEATH,
]);
