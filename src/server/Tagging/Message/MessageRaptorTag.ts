import { TagMessageByTexts } from './TagMessageByTexts';
import { MESSAGE_TAG_RAPTOR } from '../../Dependency/Tags';

export const MessageRaptorTag = new TagMessageByTexts([
  'raptor',
  'trax',
  't-rax',
  'trex',
  't-rex',
  'dino',
],                                                    [
  MESSAGE_TAG_RAPTOR,
]);
