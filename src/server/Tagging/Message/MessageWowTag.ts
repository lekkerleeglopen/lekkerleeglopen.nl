import { TagMessageByTexts } from './TagMessageByTexts';
import { MESSAGE_TAG_WOW } from '../../Dependency/Tags';

export const MessageWowTag = new TagMessageByTexts(
  [
    'wow',
    'lightsaber',
    'wauw',
  ],
  [
    MESSAGE_TAG_WOW,
  ],
);
