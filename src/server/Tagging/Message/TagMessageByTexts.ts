import { MessageTagService } from '../MessageTagService';
import { MessageVisitor } from '../MessageVisitor';

export class TagMessageByTexts implements MessageTagService {
  private words: string[];

  constructor(texts: string[], private tags: string[], private metadata: { [key: string]: any } = {}) {
    this.words = texts.map((word) => word.toLowerCase());
  }

  public tagMessage(tag: MessageVisitor): void {
    this.words.forEach((word) => {
      if (tag.messageToLowerCase.indexOf(word) >= 0) {
        tag.addTags(this.tags, this.metadata);
      }
    });
  }

}
