import { Set, List, Map } from 'immutable';
import { TaggedText } from '../../client/State/TaggedText';
import { TagEqualTextUnique } from './Text/TagEqualTextUnique';

/**
 * All functions are case-insensitive.
 */
export class TaggedTextBuilder {

  public readonly textToLowerCase: string;
  private taggedTexts: TaggedText;

  constructor(public readonly text: string) {
    this.taggedTexts = new TaggedText({ parts: List([this.text]) });
    this.textToLowerCase = this.text.toLowerCase();
  }

  /**
   * Tag a slice of the text.
   */
  public tagSlice(start: number, length: number, tags: string[], metadata: { [key: string]: any } = {}): this {
    if (this.text.length < (start + length)) {
      throw new Error('Text does not contain part');
    }
    this.taggedTexts = this.taggedTexts.tagSlice(start, start + length, Set(tags), Map(metadata)) as TaggedText;
    return this;
  }

  /**
   * Keep in mind all occurrences of the text will tagged the same.
   */
  public tagText(needle: string, tags: string[], metadata: { [key: string]: any } = {}): this {
    if (this.textToLowerCase.indexOf(needle.toLowerCase()) < 0) {
      throw new Error('Text does not contain tagged part');
    }
    TagEqualTextUnique.create(needle,  (tag) => tag(tags, metadata)).tagText(this);
    return this;
  }

  public getTaggedText(): TaggedText {
    return this.taggedTexts;
  }
}
