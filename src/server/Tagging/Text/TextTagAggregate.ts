import { TaggedTextBuilder } from '../TaggedTextBuilder';
import { TextTagService } from '../TextTagService';
import { className } from '../../className';

export class TextTagAggregate implements TextTagService {

  constructor(private services: TextTagService[]) {
    services.forEach((service) => {
      if (typeof service.tagText !== 'function') {
        throw new Error(`Service "${className(service)}" should implement tagText`);
      }
    });
  }

  public tagText(builder: TaggedTextBuilder): Promise<void> {
    return Promise.all(this.services.map((service) => {
      return Promise.resolve(service.tagText(builder));
    })) as Promise<any>;
  }

}
