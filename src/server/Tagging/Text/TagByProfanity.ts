import { TaggedTextBuilder } from '../TaggedTextBuilder';
import { TextTagService } from '../TextTagService';
import { TagByTexts } from './TagByTexts';
import { TEXT_TAG_PROFANITY } from '../../Dependency/Tags';
import { TextTagAggregate } from './TextTagAggregate';

/* tslint:disable:no-var-requires */
const en = require('./profanity/en.json');
const nl = require('./profanity/nl.json');

/**
 * This can possible be improved by some algorithm or external service.
 *
 * Uses the profanity json from:
 * https://www.npmjs.com/package/profam
 */
export class TagByProfanity implements TextTagService {

  private all = new TextTagAggregate([
    new TagByTexts(en, [TEXT_TAG_PROFANITY]),
    new TagByTexts(nl, [TEXT_TAG_PROFANITY]),
  ]);

  public tagText(builder: TaggedTextBuilder) {
    return this.all.tagText(builder);
  }

}
