import { TaggedTextBuilder } from '../TaggedTextBuilder';
import { TextTagService } from '../TextTagService';

/**
 * This can possible be improved by some algorithm or external service.
 */
export class TagByTexts implements TextTagService {

  private readonly words: string[];

  constructor(words: string[], private tags: string[], private metadata: {[key: string]: any} = {}) {
    this.words = words.map((text) => text.toLowerCase());
  }

  public tagText(builder: TaggedTextBuilder): void {
    this.words.forEach((word) => {
      if (builder.textToLowerCase.indexOf(word) >= 0) {
        builder.tagText(word, this.tags, this.metadata);
      }
    });
  }

}
