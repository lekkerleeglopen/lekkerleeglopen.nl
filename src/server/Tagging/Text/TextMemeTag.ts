import { TEXT_TAG_MEME } from '../../Dependency/Tags';
import { TagByTexts } from './TagByTexts';

export const TextMemeTag = new TagByTexts(
  [
    'not that bad',
    'is great',
    'is wonderful',
    'is your friend',
    'are great',
    'are wonderful',
    'are your friends',
  ],
  [
    TEXT_TAG_MEME,
  ],
);
