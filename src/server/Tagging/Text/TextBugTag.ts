import { TEXT_TAG_BUG } from '../../Dependency/Tags';
import { TagByTexts } from './TagByTexts';

export const TextBugTag = new TagByTexts([
  'bug',
  'coding mistake',
  'typo',
  'insect',
],                                       [
  TEXT_TAG_BUG,
]);
