import { TaggedTextBuilder } from '../TaggedTextBuilder';
import { TextTagService } from '../TextTagService';
import { TEXT_TAG_UPPERCASE } from '../../Dependency/Tags';

export class TagWordsUppercase implements TextTagService {
  private tags = [TEXT_TAG_UPPERCASE];

  public tagText(builder: TaggedTextBuilder): void {
    builder
      .text
      .split(' ')
      .forEach((text) => {
        if (text !== '' && text === text.toUpperCase()) {
          builder.tagText(text, this.tags);
        }
      });
  }

}
