import { TagByTexts } from './TagByTexts';
import { TEXT_TAG_PROGRAMMING } from '../../Dependency/Tags';

export const CodingTags = new TagByTexts([
  'javascript',
  'c++',
  'php',
  'typescript',
],                                       [
  TEXT_TAG_PROGRAMMING,
]);
