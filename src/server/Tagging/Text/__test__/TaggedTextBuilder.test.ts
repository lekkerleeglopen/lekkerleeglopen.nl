import 'jest';
import { TaggedTextBuilder } from '../../TaggedTextBuilder';
import { TaggedText } from '../../../../client/State/TaggedText';
import { List, Map, Set } from 'immutable';

describe('TaggedTextBuilder', () => {

  const text = 'Writing test sucks very hard';

  it('Should be able to add not tags to a text', () => {
    const builder = new TaggedTextBuilder(text);
    expect(builder.getTaggedText().withoutRandom()).toMatchSnapshot();
  });

  describe('Tag by text', () => {

    it('Should be able to add tags to the full text', () => {
      const builder = new TaggedTextBuilder(text);
      builder.tagText(text, ['testing']);
      expect(builder.getTaggedText().withoutRandom()).toMatchSnapshot();
    });

    it('Should be able to add tags to words', async () => {
      const builder = new TaggedTextBuilder(text);
      builder.tagText('sucks', ['swear']);
      expect(builder.getTaggedText().withoutRandom()).toMatchSnapshot();
    });

    it('Should be able to add tags to multiple words', async () => {
      const builder = new TaggedTextBuilder(text);
      builder.tagText('sucks', ['swear']);
      builder.tagText('very', ['superlative']);
      expect(builder.getTaggedText().withoutRandom()).toMatchSnapshot();
    });

    it('Should be able to add tags to same words', async () => {
      const builder = new TaggedTextBuilder(text);
      builder.tagText('very hard', ['superlative']);
      builder.tagText('very hard', ['lame']);
      expect(builder.getTaggedText().withoutRandom()).toMatchSnapshot();
    });

    it('Should be able to add tags to multiple words and sentences', async () => {
      const largeText = `Writing test sucks very hard, but it's recommended for all code`;
      const builder = new TaggedTextBuilder(largeText);
      builder.tagText(largeText, ['testing']);
      builder.tagText('very hard', ['superlative']);
      builder.tagText('very hard', ['lame']);
      builder.tagText('sucks', ['swear']);
      builder.tagText('very', ['superlative']);
      builder.tagText('very', ['lame']);
      builder.tagText('code', ['coding']);

      const taggedText = builder.getTaggedText();
      expect(taggedText.toString()).toEqual(largeText);
      expect(builder.getTaggedText().withoutRandom()).toMatchSnapshot();
    });
  });

  describe('Tag by slice', () => {
    it('Should be able to add tags to the full text', () => {
      const builder = new TaggedTextBuilder(text);
      builder.tagSlice(0, text.length, ['testing'], { hallo: 'goodbye' });
      const taggedText = builder.getTaggedText();
      expect(taggedText.withoutRandom()).toEqual(new TaggedText({
        parts: List([text]),
        tags: Set(['testing']),
        metadata: Map({ hallo: 'goodbye' }),
        random: 0,
      }));
    });

    it('Should be able to add tags to words', async () => {
      const builder = new TaggedTextBuilder('Writing test sucks very hard');
      const begin = 'Writing test ';
      const needle = 'sucks';
      const end = ' very hard';
      builder.tagSlice(begin.length, needle.length, ['swear'], { foo: 'bar' });
      const taggedText = builder.getTaggedText();
      expect(taggedText.parts.get(0)).toEqual(begin);
      expect((taggedText.parts.get(1) as any).withoutRandom()).toEqual(new TaggedText({
        parts: List([needle]),
        tags: Set(['swear']),
        metadata: Map({ foo: 'bar' }),
        random: 0,
      }));
      expect(taggedText.parts.get(2)).toEqual(end);
    });

    it('Should be able to add tags to same words', async () => {
      const builder = new TaggedTextBuilder('Writing test sucks very hard');
      builder.tagSlice(19, 4, ['superlative'], { first: 1 });
      let taggedText = builder.getTaggedText();
      expect((taggedText.parts.get(1)as any).withoutRandom()).toEqual(new TaggedText({
        parts: List(['very']),
        tags: Set(['superlative']),
        metadata: Map({ first: 1 }),
        random: 0,
      }));
      builder.tagSlice(19, 4, ['lame'], { second: 2 });
      taggedText = builder.getTaggedText();
      expect((taggedText.parts.get(1)as any).withoutRandom()).toEqual(new TaggedText({
        parts: List(['very']),
        tags: Set(['superlative', 'lame']),
        metadata: Map({ first: 1, second: 2 }),
        random: 0,
      }));
    });

    it('Should be able to tag nested text', async () => {
      const largeText = `Writing test sucks very hard, but it's recommended for all code`;
      const builder = new TaggedTextBuilder(largeText);
      builder.tagSlice(8, 43, ['big part']);
      builder.tagSlice(24, 4, ['hard']);
      builder.tagSlice(59, 4, ['code']);
      const taggedText = builder.getTaggedText();
      expect(taggedText.toString()).toEqual(largeText);
      expect(builder.getTaggedText().withoutRandom()).toMatchSnapshot();
    });
  });

});
