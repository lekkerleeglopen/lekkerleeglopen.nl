import { multipleIndexOf } from '../../multipleIndexOf';

describe('findIndexOfMultiple', () => {

  it('Should find nothing when there are none', () => {
    expect(multipleIndexOf('test 1 test 2 test 3 test', 'none')).toEqual([]);
  });

  it('Can return single index', () => {
    expect(multipleIndexOf('test', 'test')).toEqual([0]);
  });

  it('Should find multiple index', () => {
    expect(multipleIndexOf('test 1 test 2 test 3 test', 'test')).toEqual([0, 7, 14, 21]);
  });

  it('Should find multiple index', () => {
    expect(multipleIndexOf('exception: bug bug bug bug ', 'bug')).toEqual([11, 15, 19, 23]);
  });

  it('Should ignore empty string', () => {
    expect(multipleIndexOf('some text', '')).toEqual([]);
  });

});
