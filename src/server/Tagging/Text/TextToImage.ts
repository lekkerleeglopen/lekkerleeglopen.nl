import { TaggedTextBuilder } from '../TaggedTextBuilder';
import { TextTagService } from '../TextTagService';
import { TagByTexts } from './TagByTexts';
import { TEXT_TAG_TO_IMAGE } from '../../Dependency/Tags';
import * as fs from 'fs';
import * as path from 'path';
import { TextTagAggregate } from './TextTagAggregate';

const images = fs.readdirSync(path.join(__dirname, '..', '..', '..', '..', '/public/textToImages'));

/**
 * This can possible be improved by some algorithm or external service.
 *
 * Uses the profanity json from:
 * https://www.npmjs.com/package/profam
 */
export class TextToImage implements TextTagService {
  private tags: TextTagAggregate;

  constructor() {
    this.tags = new TextTagAggregate(images.map((file) => {
      return new TagByTexts([file.split('.')[0]], [TEXT_TAG_TO_IMAGE], { img: `/public/textToImages/${file}` });
    }))
  }

  public tagText(builder: TaggedTextBuilder) {
    return this.tags.tagText(builder);
  }

}
