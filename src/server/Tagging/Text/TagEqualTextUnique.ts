import { TextTagService } from '../TextTagService';
import { multipleIndexOf } from '../multipleIndexOf';
import { TaggedTextBuilder } from '../TaggedTextBuilder';
import { TextTagAggregate } from './TextTagAggregate';

export type TagFactory = (tag: (tags: string[], metadata?: { [key: string]: any }) => void) => Promise<any> | void;

/**
 * Use this if you want to tag the same word differently.
 */
export class TagEqualTextUnique implements TextTagService {

  public static createForMultipleTexts(texts: string[], factory: TagFactory) {
    return new TextTagAggregate(texts.map((text) => {
      return new this(text, factory);
    }))
  }

  public static create(text: string, factory: TagFactory) {
    return new this(text, factory);
  }

  private readonly needleLength;

  constructor(private needle: string, private factory: TagFactory) {
    this.needleLength = needle.length;
  }

  public tagText(builder: TaggedTextBuilder): Promise<any> | void {
    const indexes = multipleIndexOf(builder.text.toLowerCase(), this.needle.toLowerCase());
    return Promise.all(indexes.map((index) => {
      return Promise.resolve(this.factory((tags, metadata) => {
        return builder.tagSlice(index, this.needleLength, tags, metadata);
      }));
    }));
  }

}
