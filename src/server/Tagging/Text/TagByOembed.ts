import { TaggedTextBuilder } from '../TaggedTextBuilder';
import { TextTagService } from '../TextTagService';
import * as oembed from 'oembed-parser';
import { TEXT_TAG_OEMBED } from '../../Dependency/Tags';

// tslint:disable-next-line
const urlRegex = require('url-regex');

export class TagByOembed implements TextTagService {

  public async tagText(builder: TaggedTextBuilder) {
    const urls = builder.text.match(urlRegex());
    if (!urls) {
      return;
    }
    for (const url of urls) {
      if (!oembed.hasProvider(url)) {
        builder.tagSlice(builder.text.indexOf(url), url.length, [TEXT_TAG_OEMBED]);
        continue;
      }
      try {
        const json = await oembed.extract(url);
        builder.tagSlice(builder.text.indexOf(url), url.length, [TEXT_TAG_OEMBED], { oembed: json });
      } catch (error) {
        builder.tagSlice(builder.text.indexOf(url), url.length, [TEXT_TAG_OEMBED], { error: error.toString() });
      }
    }
  }

}
