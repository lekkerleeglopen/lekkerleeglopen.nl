import { TextTagService } from '../TextTagService';
import { TaggedTextBuilder } from '../TaggedTextBuilder';
import { TEXT_TAG_KITTEN } from '../../Dependency/Tags';
import { default as fetch } from 'node-fetch';
import { TagEqualTextUnique } from './TagEqualTextUnique';

export class TextKittenTag implements TextTagService {

  private kittens = [
    'pussy',
    'cat',
    'kitten',
    'poes',
    'kat',
  ];

  private tagService = TagEqualTextUnique
    .createForMultipleTexts(
      this.kittens,
      this.tagFactory.bind(this),
    );

  public tagText(builder: TaggedTextBuilder) {
    return this.tagService.tagText(builder);
  }

  private async tagFactory(tag) {
    const url = await this.generateRandomKittenUrl();
    tag([TEXT_TAG_KITTEN], { img: url });
  }

  private async generateRandomKittenUrl() {
    const id = Math.random().toString(36).substr(2, 16);
    const result: Response = await fetch(`http://thecatapi.com/api/images/get?format=src&_cb=${id}`, { redirect: 'manual' });
    const location = result.headers.get('location');
    if (location) {
      return location;
    }
    // If we don't get a image.. just serve a random one.
    return 'http://thecatapi.com/api/images/get?format=src';
  }
}
