import { TaggedTextBuilder } from './TaggedTextBuilder';

export interface TextTagService {

  tagText(builder: TaggedTextBuilder): Promise<any> | void;

}
