import * as React from 'react';
import { Controller, Get, Inject } from '@nestjs/common';
import { renderToString } from 'react-dom/server';
import { ManifestReaderInterface } from '../AssetService/ManifestReaderInterface';
import { IndexView } from '../view/IndexView';

@Controller()
export class IndexController {

  constructor(@Inject('CachedManifestReader') private reader: ManifestReaderInterface) {
  }

  @Get()
  public root(): string {
    const collection = this.reader.read();
    const jsAssets = collection.filterJSAssets().orderBy(['manifest', 'vendor', 'main']);
    return renderToString((
      <IndexView
        js={jsAssets.values()}
        css={collection.filterCSSAssets().values()}
      />
    ));
  }
}
