import { ManifestReader } from '../AssetService/ManifestReader';
import { CachedManifestReaderDecorator } from '../AssetService/CachedManifestReaderDecorator';

export const cachedManifestReaderDependency = {
  provide: 'CachedManifestReader',
  useFactory: (reader: ManifestReader) => {
    // We don't cache the reader, to know the changes from webpack watch.
    if (process.env.NODE_ENV !== 'development') {
      return new CachedManifestReaderDecorator(reader);
    }
    return reader;
  },
  inject: [ManifestReader],
};
