import { chatReducer } from '../../client/Reducer/chatReducer';
import { applyMiddleware, combineReducers, createStore, Reducer } from 'redux';
import { ServerState } from '../State/ServerState';
import { ChatState } from '../../client/State/ChatState';
import { combineEpics, createEpicMiddleware } from 'redux-observable';
import { timerEpic } from '../../client/Epic/timerEpic';

export const serverStoreDependency = {
  provide: 'ServerStore',
  useFactory: () => {
    const reducers: Reducer<ServerState> = combineReducers<ServerState>({
      chat: chatReducer,
    });
    const defaultState: ServerState = {
      chat: new ChatState(),
    };
    const middleware = [
      createEpicMiddleware(
        combineEpics(
          timerEpic,
        ),
      ),
    ];
    return createStore<ServerState>(reducers as any, defaultState, applyMiddleware(...middleware));
  },
};
