
export const MESSAGE_BLUE_SCREEN_OF_DEATH = 'bsod';
export const MESSAGE_SERVER_ERROR = 'server_error';
export const MESSAGE_TAG_RAPTOR = 'raptor';
export const MESSAGE_TAG_WOW = 'wow';
export const MESSAGE_TAG_CLIPPY_ANIMATE = 'clippy_animate';

export const TEXT_TAG_CLIPPY = 'clippy';
export const TEXT_TAG_BUG = 'bug';
export const TEXT_TAG_PROGRAMMING = 'programming';
export const TEXT_TAG_KITTEN = 'kitten';
export const TEXT_TAG_PROFANITY = 'profanity';
export const TEXT_TAG_TO_IMAGE = 'to_image';
export const TEXT_TAG_UPPERCASE = 'uppercase';
export const TEXT_TAG_MEME = 'meme';
export const TEXT_TAG_OEMBED = 'oembed';
