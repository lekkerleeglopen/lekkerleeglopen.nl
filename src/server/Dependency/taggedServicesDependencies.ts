import { TagByProfanity } from '../Tagging/Text/TagByProfanity';
import { TextToImage } from '../Tagging/Text/TextToImage';
import { CodingTags } from '../Tagging/Text/CodingTags';
import { TextTagAggregate } from '../Tagging/Text/TextTagAggregate';
import { TagWordsUppercase } from '../Tagging/Text/TagWordsUppercase';
import { TextBugTag } from '../Tagging/Text/TextBugTag';
import { MessageTagAggregate } from '../Tagging/Message/MessageTagAggregate';
import { MessageRaptorTag } from '../Tagging/Message/MessageRaptorTag';
import { TextKittenTag } from '../Tagging/Text/TextKittenTag';
import { MessageStackTraceTag } from '../Tagging/Message/MessageStackTraceTag';
import { MessageTagService } from '../Tagging/MessageTagService';
import { TextTagService } from '../Tagging/TextTagService';
import { TextMemeTag } from '../Tagging/Text/TextMemeTag';
import { MessageWowTag } from '../Tagging/Message/MessageWowTag';
import { MessageClippyTags } from '../Tagging/Message/MessageClippyTags';
import { TagByOembed } from '../Tagging/Text/TagByOembed';

const messageClasses: Array<new (...args: any[]) => MessageTagService> = [
  MessageClippyTags,
];

const messageValueObjects: MessageTagService[] = [
  MessageRaptorTag,
  MessageWowTag,
  MessageStackTraceTag,
];

const textClasses: Array<new (...args: any[]) => TextTagService> = [
  TagByProfanity,
  TextToImage,
  TagWordsUppercase,
  TextKittenTag,
  MessageClippyTags,
  TagByOembed,
];
const textValueObjects: TextTagService[] = [
  CodingTags,
  TextBugTag,
  TextMemeTag,
];

export const taggedServicesDependencies = [
  ...messageClasses,
  ...messageValueObjects.map((value) => {
    return { provide: value, useValue: value };
  }),
  ...textClasses,
  ...textValueObjects.map((value) => {
    return { provide: value, useValue: value };
  }),
  {
    provide: 'MessageTagService',
    useFactory: (...taggedServices) => {
      return new MessageTagAggregate(taggedServices);
    },
    inject: [].concat(messageClasses, messageValueObjects),
  },
  {
    provide: 'TextTaggedService',
    useFactory: (...taggedServices) => {
      return new TextTagAggregate(taggedServices);
    },
    inject: [].concat(textClasses, textValueObjects),
  },
];
