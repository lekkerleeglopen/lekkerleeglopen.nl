/**
 * Bootstrap file for server nest application.
 */

import { NestFactory } from '@nestjs/core';

import { ApplicationModule } from './ApplicationModule';
import * as path from 'path';
import * as express from 'express';
import { INestApplication } from '@nestjs/common/interfaces/nest-application.interface';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  process.env.port = process.env.port ? process.env.port : '4000';
  const app: INestApplication = await NestFactory.create(ApplicationModule) as any;

  const expiresStatics = {
    maxAge: 86400000,
    setHeaders: (res) => {
      res.setHeader('Cache-Control', 'public, max-age=' + 86400000);
      res.setHeader('Expires', new Date(Date.now() + 2592000000 * 30).toUTCString());
    },
  };
  app.use(express.static(path.join(__dirname, '/../../build/client'), expiresStatics));
  app.use('/public', express.static(path.join(__dirname, '/../../public'), expiresStatics));

  return app;
}

bootstrap().then(async (app: INestApplication) => {
  const logger = app.get(Logger);
  await app.listen(parseInt(process.env.port, 10));
  logger.log(`Application started http://localhost:${process.env.port}`);
}).catch((error: any) => {
  process.stderr.write(`${error}\n`);
});
