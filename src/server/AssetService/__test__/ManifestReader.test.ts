import 'jest';
import * as mockfs from 'mock-fs';
import { ManifestReader } from '../ManifestReader';

describe('ManifestReader', () => {

  beforeEach(() => {
    mockfs({
      'path/to/manifest.json': JSON.stringify({
        'main.js': '/main.a346799bd77fb1777a75.js',
        'main.js.map': '/main.a346799bd77fb1777a75.js.map',
        'style.css': '/style.d41d8cd98f00b204e9800998ecf8427e.css',
        'style.css.map': '/style.d41d8cd98f00b204e9800998ecf8427e.css.map',
      }),
    });
  });

  afterEach(() => {
    mockfs.restore();
  });

  it('Should return collection of assets', () => {
    const factory = new ManifestReader('path/to/manifest.json');
    const container = factory.read();

    expect(container.filterJSAssets().values()).toEqual(['/main.a346799bd77fb1777a75.js']);
    expect(container.filterCSSAssets().values()).toEqual(['/style.d41d8cd98f00b204e9800998ecf8427e.css']);
  });
});
