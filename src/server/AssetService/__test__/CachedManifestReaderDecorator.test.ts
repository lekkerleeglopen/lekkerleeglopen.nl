import 'jest';
import { ManifestReader } from '../ManifestReader';
import { CachedManifestReaderDecorator } from '../CachedManifestReaderDecorator';

describe('CachedManifestReaderDecorator', () => {

  it('Should return collection of assets, cached', () => {
    const readMock = jest.fn().mockReturnValue('the cached value');
    const mock: ManifestReader = {
      read: readMock,
    } as any;

    const decorated = new CachedManifestReaderDecorator(mock);
    expect(decorated.read()).toEqual('the cached value');
    expect(decorated.read()).toEqual('the cached value');
    expect(readMock.mock.calls.length).toBe(1);
  });
});
