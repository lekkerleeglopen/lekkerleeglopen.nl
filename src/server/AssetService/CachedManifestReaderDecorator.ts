import { Component } from '@nestjs/common';
import { ManifestReaderInterface } from './ManifestReaderInterface';
import { AssetCollection } from './Collection/AssetCollection';

@Component()
export class CachedManifestReaderDecorator implements ManifestReaderInterface {
  private memory: AssetCollection = null;

  constructor(private reader: ManifestReaderInterface) {

  }

  /**
   * Factory.
   */
  public read(): AssetCollection {
    if (this.memory == null) {
      this.memory = this.reader.read();
    }
    return this.memory;
  }

}
