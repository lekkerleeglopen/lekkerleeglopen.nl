import * as fs from 'fs';
import { Component, Inject } from '@nestjs/common';
import { ManifestReaderInterface } from './ManifestReaderInterface';
import { AssetCollection } from './Collection/AssetCollection';

@Component()
export class ManifestReader implements ManifestReaderInterface {

  constructor(@Inject('manifest_file') private manifestPath: string) {

  }

  /**
   * Factory.
   */
  public read(): AssetCollection {
    const manifest = fs.readFileSync(this.manifestPath).toString();
    const encoded: { [key: string]: string } = JSON.parse(manifest);
    const assets: string[] = [];
    const keys = Object.keys(encoded);
    for (const key of keys) {
      const file = encoded[key];
      if (file.match(/\.(js|css)$/) !== null) {
        assets.push(file);
      }
    }
    return new AssetCollection(assets);
  }

}
