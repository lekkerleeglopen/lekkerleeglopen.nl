/**
 * Collection of assets.
 */
export class AssetCollection {

  constructor(private assets: string[]) {
    this.assets = this.values();
  }

  public values(): string[] {
    return this.assets.concat();
  }

  public orderBy(names: string[]): AssetCollection {
    return new AssetCollection(this.values().sort((a, b) => {
      const posA = names.findIndex((key) => {
        return a.indexOf(key) >= 0;
      });
      const posB = names.findIndex((key) => {
        return b.indexOf(key) >= 0;
      });
      if (posA === posB) {
        return 0;
      }
      return posA > posB ? 1 : -1;
    }));
  }

  public filterJSAssets(): AssetCollection {
    return this.filter(file => file.match(/.js$/) != null);
  }

  public filterCSSAssets(): AssetCollection {
    return this.filter(file => file.match(/.css$/) != null);
  }

  protected filter(accepts: (asset: string) => boolean): AssetCollection {
    return new AssetCollection(this.assets.filter(accepts));
  }

}
