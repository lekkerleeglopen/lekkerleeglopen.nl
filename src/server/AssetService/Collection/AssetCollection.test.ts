import 'jest';
import { AssetCollection } from './AssetCollection';

describe('AssetCollection', () => {

  describe('Is immutable', () => {
    it('Arguments are not by reference', () => {
      const strings = ['hi'];
      const collection = new AssetCollection(strings);

      expect(collection.values()).toEqual(strings);
      expect(strings === collection.values()).toBeFalsy();

      strings.push('test');
      expect(collection.values()).not.toEqual(strings);
    });

    it('To value is not by reference', () => {
      const strings = ['hi'];
      const collection = new AssetCollection(strings);

      expect(collection.values()).toEqual(strings);

      collection.values().push('test');

      expect(collection.values()).toEqual(strings);
    });
  });

  describe('Can filter on file type', () => {
    it('by JS', () => {
      const assets = ['test.js', 'style.css'];
      const collection = new AssetCollection(assets);
      expect(collection.filterJSAssets().values()).toEqual(['test.js']);
    });

    it('by CSS', () => {
      const assets = ['test.js', 'style.css'];
      const collection = new AssetCollection(assets);
      expect(collection.filterCSSAssets().values()).toEqual(['style.css']);
    });
  });
});
