import { AssetCollection } from './Collection/AssetCollection';

export interface ManifestReaderInterface {

  read(): AssetCollection;

}
