import { Test } from '@nestjs/testing';
import { ApplicationModule } from './ApplicationModule';
import { TestingModuleBuilder } from '@nestjs/testing/testing-module.builder';
import { ChatGateway } from './Gateway/ChatGateway';

export function createTestApplicationModule(): TestingModuleBuilder {
  return Test.createTestingModule({
    imports: [ApplicationModule],
    exports: [ApplicationModule],
    components: [],
  }).overrideComponent(ChatGateway).useValue({
    sendMessage: jest.fn(),
  });
}
