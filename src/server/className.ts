
export function className(instance: any): string {
  const { constructor } = Object.getPrototypeOf(instance);
  return constructor.name as string;
}
