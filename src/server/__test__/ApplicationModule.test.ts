import 'jest';
import { ApplicationModule } from '../ApplicationModule';
import { createTestApplicationModule } from '../createTestApplicationModule';
import { ManifestReader } from '../AssetService/ManifestReader';
import { CachedManifestReaderDecorator } from '../AssetService/CachedManifestReaderDecorator';

describe('ApplicationModule', () => {

  let app;

  beforeEach(async () => {
    const module = await createTestApplicationModule().compile();
    app = module.createNestApplication();
  });

  it('Should contain ManifestReader', async () => {
    const actual = app.select(ApplicationModule).get(ManifestReader);
    expect(actual).toBeInstanceOf(ManifestReader);
  });

  it('Should contain AssetContainer mode', async () => {
    const actual = app.select(ApplicationModule).get('CachedManifestReader');
    expect(actual).toBeInstanceOf(CachedManifestReaderDecorator);
  });

});
