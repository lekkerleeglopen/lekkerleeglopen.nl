import 'jest';
import { ApplicationModule } from '../ApplicationModule';
import { createTestApplicationModule } from '../createTestApplicationModule';
import { TextTagService } from '../Tagging/TextTagService';
import { TaggedTextBuilder } from '../Tagging/TaggedTextBuilder';

describe('TagTestService', () => {

  let app;

  beforeEach(async () => {
    const module = await createTestApplicationModule().compile();
    app = module.createNestApplication();
  });

  it('Should be able to tag a message', async () => {

    const textTagService: TextTagService = app.select(ApplicationModule).get('TextTaggedService');

    const builder = new TaggedTextBuilder('exception: bug bug bug bug ');
    textTagService.tagText(builder);
    expect(builder.getTaggedText().withoutRandom()).toMatchSnapshot();
  });

});
