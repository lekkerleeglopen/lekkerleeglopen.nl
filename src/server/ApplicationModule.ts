/**
 * Nest app module.
 */

import { Logger, Module } from '@nestjs/common';

import { IndexController } from './Controller/IndexController';
import { ManifestReader } from './AssetService/ManifestReader';
import { cachedManifestReaderDependency } from './Dependency/cachedManifestReaderDependency';
import { ChatGateway } from './Gateway/ChatGateway';
import { SerializerFactory } from '../client/SerializerFactory';
import { serverStoreDependency } from './Dependency/serverStoreDependency';
import { taggedServicesDependencies } from './Dependency/taggedServicesDependencies';
import * as StackUtils from 'stack-utils';
import { TranslateService } from './Translate/TranslateService';
import { MessageClippyTags } from './Tagging/Message/MessageClippyTags';

/**
 * Nest module config.
 */
@Module({
  imports: [],
  controllers: [IndexController],
  exports: [],
  components: [
    { provide: 'manifest_file', useValue: __dirname + '/../../build/client/manifest.json' },
    ManifestReader,
    ChatGateway,
    {
      provide: 'serializer',
      useFactory: () => {
        const factory = new SerializerFactory();
        return factory.create();
      },
    },
    {
      provide: StackUtils,
      useValue: new StackUtils({ cwd: process.cwd(), internals: StackUtils.nodeInternals() }),
    },
    {
      provide: Logger,
      useFactory: () => {
        return new Logger('Application');
      },
    },
    cachedManifestReaderDependency,
    serverStoreDependency,
    ...taggedServicesDependencies,
    TranslateService,
  ],
})
export class ApplicationModule {
  constructor(private readonly clippy: MessageClippyTags) {
  }

  public async onModuleInit() {
    await this.clippy.load();
  }
}
