import { ChatState } from '../../client/State/ChatState';

export interface ServerState {
  chat: ChatState;
}
