import {
  WebSocketGateway,
  WebSocketServer,
  SubscribeMessage,
  OnGatewayDisconnect,
  OnGatewayConnection,
} from '@nestjs/websockets';
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/map';
import * as uuid from 'uuid/v4';
import { Inject, Logger } from '@nestjs/common';
import { SerializerInterface } from '../../shared/Serializer/SerializerInterface';
import { Message } from '../../client/State/Message';
import { ServerStore } from '../ServerStore';
import { receiveMessageFromServer } from '../../client/Action/chat';
import { TextTagService } from '../Tagging/TextTagService';
import { TaggedTextBuilder } from '../Tagging/TaggedTextBuilder';
import { MessageTagService } from '../Tagging/MessageTagService';
import { MessageVisitor } from '../Tagging/MessageVisitor';
import * as StackUtils from 'stack-utils';
import { List, Set, Map } from 'immutable';
import { MESSAGE_SERVER_ERROR } from '../Dependency/Tags';
import { TaggedText } from '../../client/State/TaggedText';

@WebSocketGateway({
  namespace: 'chat',
})
export class ChatGateway implements OnGatewayConnection,
  OnGatewayDisconnect {
  @WebSocketServer() private server: SocketIO.Server;

  constructor(@Inject(Logger) private logger: Logger,
              @Inject('serializer') private serializer: SerializerInterface,
              @Inject('ServerStore') private store: ServerStore,
              @Inject('TextTaggedService') private textTaggedService: TextTagService,
              @Inject('MessageTagService') private messageTagService: MessageTagService,
              private stackTraceUtils: StackUtils) {
  }

  @SubscribeMessage('message')
  public async onMessage(client, rawMassage: any) {
    this.logger.log(`Client '${client.id}' has send a message`);
    if (typeof rawMassage !== 'string') {
      this.logger.warn(`Message is not a string`);
      return;
    }
    const textMessage = rawMassage.length > 1024 ? rawMassage.slice(0, 1024) : rawMassage;
    const builder = new TaggedTextBuilder(textMessage);
    try {
      await this.textTaggedService.tagText(builder);
      const taggedText = builder.getTaggedText();
      const messageVisitor = new MessageVisitor(textMessage);
      await this.messageTagService.tagMessage(messageVisitor);
      const message = new Message({
        postedOn: new Date(),
        text: taggedText,
        uuid: uuid(),
        tags: messageVisitor.getTags(),
        metadata: messageVisitor.getMetadata(),
      });
      this.sendMessage(message);
    } catch (e) {
      this.logger.error(e);
      try {
        const errorMessage = this.createErrorMessage(e, textMessage);
        this.sendMessage(errorMessage);
      } catch (lastError) {
        this.logger.error(lastError);
      }
    }
  }

  public sendMessage(message: Message): void {
    this.store.dispatch(receiveMessageFromServer(message));
    this.server.emit('message', this.serializer.serialize(message));
  }

  public handleConnection(client) {
    this.logger.log(`Socket id ${client.id} opened`);
    client.emit('initialState', this.serializer.serialize(this.store.getState().chat));
  }

  public handleDisconnect(client: SocketIO.Client) {
    this.logger.log(`Socket id ${client.id} closed`);
  }

  private createErrorMessage(e, textMessage) {
    const clean = typeof e === 'object' && e.stack ? this.stackTraceUtils.clean(e.stack) : e.toString();
    const messages = typeof clean === 'string' ? clean.split('\n') : clean;
    return new Message({
      postedOn: new Date(),
      text: (new TaggedText({
        parts: List([e.toString()].concat(messages)),
      })).withoutEmptyTextParts(),
      uuid: uuid(),
      tags: Set([MESSAGE_SERVER_ERROR]),
      metadata: Map({ textMessage }),
    });
  }

}
