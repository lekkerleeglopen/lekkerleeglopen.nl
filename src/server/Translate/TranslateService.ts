import * as translate from 'google-translate-api';
import { Component } from '@nestjs/common';

@Component()
export class TranslateService {

  public async translateEnglishToDutch(text: string) {
    return this.translate('en', 'nl', text)
  }

  public async translateDutchToEnglish(text: string) {
    return this.translate('nl', 'en', text)
  }

  public async translate(from: string, to: string, text: string): Promise<string> {
    const res = await translate(text, { from, to });
    return res.text;
  }

}
