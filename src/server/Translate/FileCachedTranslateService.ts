import { Component } from '@nestjs/common';
import * as fs from 'fs';
import { TranslateService } from './TranslateService';

@Component()
export class FileCachedTranslateService {
  private translations: { [langFrom: string]: { [langTo: string]: { [text: string]: any } } } = {};

  constructor(private service: TranslateService, private file: string) {
    if (!fs.existsSync(file)) {
      this.store();
    } else {
      const content = fs.readFileSync(file);
      this.translations = JSON.parse(content.toString());
    }
  }

  public async translateEnglishToDutch(text: string) {
    return this.translate('en', 'nl', text);
  }

  public async translateDutchToEnglish(text: string) {
    return this.translate('nl', 'en', text);
  }

  public async translate(from: string, to: string, text: string): Promise<string> {
    if (this.translations[from] && this.translations[from][to] && this.translations[from][to][text]) {
      return this.translations[from][to][text];
    }
    const translated = await this.service.translate(from, to, text);
    if (!this.translations[from]) {
      this.translations[from] = {};
    }
    if (!this.translations[from][to]) {
      this.translations[from][to] = {};
    }
    this.translations[from][to][text] = translated;
    this.store();
    return translated;
  }

  private store() {
    fs.writeFileSync(this.file, JSON.stringify(this.translations));
  }

}
