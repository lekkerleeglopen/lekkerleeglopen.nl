/**
 * Component.
 */

import * as React from 'react';

/**
 * The page, rendered only server side.
 */
export class IndexView extends React.PureComponent<{ js: string[], css: string[] }, {}> {

  public render(): React.ReactNode {

    const js = this.props.js.map((file, index) => <script src={file} key={index}/>);
    const css = this.props.css.map((file, index) => <link rel="stylesheet" type="text/css" href={file} key={index}/>);

    return (
      <html lang="en">

      <head>
        <meta name="charset" content="utf-8"/>
        <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
        <title>Lekkerleeglopen.nl</title>
        <link rel="shortcut icon" type="image/x-icon" href="/public/favicon.ico" />
        <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet" />
        <link href="/public/clippy/build/clippy.css" rel="stylesheet" />
        <meta name="description" content="Lekkerleeglopen."/>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, user-scalable=0, maximum-scale=1, minimum-scale=1"
        />
        {css}
      </head>

      <body>
      <div id="app"/>
      <script src="/public/bug/bug-min.js" />
      {js}
      <script src="/public/raptorize/jquery.raptorize.1.0.js" />
      <script src="/public/clippy/build/clippy.js" />
      </body>

      </html>
    );
  }

}
