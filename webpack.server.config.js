const Encore = require("@symfony/webpack-encore");
const path = require("path");
const NodemonPlugin = require("nodemon-webpack-plugin");
const nodeExternals = require("webpack-node-externals");

Encore.reset();

const config = Encore.enableSourceMaps()
    // Convert typescript files.
    .enableTypeScriptLoader(function(tsConfig) {
        tsConfig.configFile = path.resolve(__dirname, "tsconfig.server.json");
    })
    // Show OS notifications when builds finish/fail.
    .enableBuildNotifications()

    // Node deamon for server
    .addPlugin(new NodemonPlugin({
        /// Arguments to pass to the script being watched.
        args: [],

        // Detailed log.
        verbose: true,

        // Node arguments.
        nodeArgs: ["--inspect=9229"],

        // If using more than one entry, you can specify
        // which output file will be restarted.
        script: "./index.js",
    }))

    // The project directory where all compiled assets will be stored.
    .setOutputPath("build/server/")

    // The public path used by the web server to access the previous directory.
    .setPublicPath("/")

    .addEntry("main", "./src/server/main.ts");

if (Encore.isProduction()) {
    config.cleanupOutputBeforeBuild();
    config.addLoader({
        test: /\.tsx?|\.js$/,
        exclude: /node_modules/,
        use: [
            {
                loader: "tslint-loader",
                options: {
                    configFile: "tslint.json",
                    emitErrors: true,
                    failOnHint: Encore.isProduction(),
                    typeCheck: true,
                    fix: true,
                },
            },
        ],
    });
} else {
    config.enableForkedTypeScriptTypesChecking();
}

const webpackConfig = config.getWebpackConfig();
webpackConfig.name = "server";
webpackConfig.target = "node";
webpackConfig.externals = [nodeExternals()];
module.exports = webpackConfig;
