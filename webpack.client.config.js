const Encore = require("@symfony/webpack-encore");
const path = require("path");
const webpack = require("webpack");
const CompressionPlugin = require("compression-webpack-plugin");
const BrowserSyncPlugin = require("browser-sync-webpack-plugin");
const CaseSensitivePathsPlugin = require("case-sensitive-paths-webpack-plugin");

Encore.reset();

const config = Encore.enableSourceMaps()

// Convert typescript files.
    .enableTypeScriptLoader(function(tsConfig) {
        tsConfig.configFile = path.resolve(__dirname, "tsconfig.client.json");
    })

    // Convert sass files.
    .enableSassLoader((options) => {
        // https://github.com/sass/node-sass#options.
        options.includePaths = [];
    })

    .addPlugin(new CaseSensitivePathsPlugin())

    .addPlugin(new webpack.IgnorePlugin(/server/, /server$/))
    // The project directory where all compiled assets will be stored.
    .setOutputPath("build/client/")

    // The public path used by the web server to access the previous directory.
    .setPublicPath("/")

    // this creates a 'vendor.js' file with jquery and the bootstrap JS module
    // these modules will *not* be included in page1.js or page2.js anymore
    .createSharedEntry("vendor", [
        "babel-polyfill",
        "core-js",
        "redux",
        "redux-observable",
        "redux-devtools-extension",
        "react",
        "immutable",
        "transit-js",
        "transit-immutable-js",
        "rxjs",
        "socket.io-client",
    ])

    // Will convert typescript file to build/client/main.js.
    .addEntry("main", "./src/client/main.tsx")

    .addStyleEntry("style", "./sass/style.scss");

if (!Encore.isProduction()) {
    // Show OS notifications when builds finish/fail.
    config.enableBuildNotifications();
    // Add browser sync plugin
    config.addPlugin(new BrowserSyncPlugin({
        proxy: "http://localhost:4000",
        notify: true,
        https: false,
        port: 3002,
    }));

    config.enableForkedTypeScriptTypesChecking();
}

if (Encore.isProduction()) {
    // Compress assets.
    config.addPlugin(new CompressionPlugin({
        test: /\.(js|css|jpeg|gif|svg|png)/,
        minRatio: Number.MAX_VALUE,
    }));
    config.cleanupOutputBeforeBuild();
    config.enableVersioning();
}

const webpackConfig = config.getWebpackConfig();
webpackConfig.name = "client";
webpackConfig.resolve.modules = [
    "node_modules",
    path.resolve(__dirname, "src", "client"),
    path.resolve(__dirname, "src", "shared"),
];

// Export the final configuration.
module.exports = webpackConfig;
