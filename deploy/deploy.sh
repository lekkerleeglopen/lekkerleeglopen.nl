#!/bin/bash

# change dir to ansible directory
cd $(dirname $0)

if `[ "$1" = "" ] || [ "$2" = "" ] || [ "$3" = "" ]`
then
    echo usage: $0 target build build_number
    echo example: $0 acc build.tar 123
    echo example: $0 prod build.tar 123
    exit 1
fi

set -x #echo on

# pass a timestamp that is used for every task
ansible-playbook deploy.yml --extra-vars="timestamp=`date +%d-%m-%Y_%Hh%Mm` build=$2 env=$1 build_number=$3" --limit=$1
